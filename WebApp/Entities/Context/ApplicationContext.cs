using System;
using WebApp.Modules.UserManagement.Entities;
using WebApp.Modules.UserManagement.Seeders;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using WebApp.Modules.DakarRally.Entities;

namespace WebApp.Entities.Context
{
    public class ApplicationDbContext : IdentityDbContext<User, Role, int, UserClaim, UserRole, UserLogin, RoleClaim, UserToken>
    {
        // private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly IServiceProvider _serviceProvider;
        
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options) : base(options)
        {
        }

        public DbSet<Claims> Claims { get; set; }
        public DbSet<Role> Role { get; set; }
        public DbSet<RoleClaim> RoleClaim { get; set; }
        public DbSet<UserRole> UserRole { get; set; }
        public DbSet<User> User { get; set; }
        public DbSet<VehicleModel> VehicleModel { get; set; }
        public DbSet<Vehicle> Vehicle { get; set; }
        public DbSet<Race> Race { get; set; }
        public DbSet<VehicleRace> VehicleRace { get; set; }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);
            
            RoleSeed.Seed(builder);
            UserSeed.Seed(builder);
            ClaimSeed.Seed(builder);

            SetupIdentityRelations(builder);
        }

        protected void SetupIdentityRelations(ModelBuilder builder)
        {
            builder.Entity<User>(b => {
                b.ToTable("Users");
            });

            builder.Entity<Role>(b => {
                b.ToTable("Roles");
            });

            builder.Entity<UserRole>(userRole =>
            {
                userRole.HasKey(ur => new { ur.UserId, ur.RoleId });
                userRole.HasOne(ur => ur.Role)
                    .WithMany(r => r.Users)
                    .HasForeignKey(ur => ur.RoleId)
                    .IsRequired();
                userRole.HasOne(ur => ur.User)
                    .WithMany(r => r.UserRoles)
                    .HasForeignKey(ur => ur.UserId)
                    .IsRequired();

                userRole.ToTable("UserRoles");
            });
            builder.Entity<RoleClaim>(c =>
            {
                c.HasOne(roleClaim => roleClaim.Role).WithMany(role => role.Claims).HasForeignKey(roleClaim => roleClaim.RoleId);
                c.ToTable("RoleClaim");
            });
            builder.Entity<UserClaim>(c =>
            {
                c.HasOne(userClaim => userClaim.User).WithMany(user => user.Claims).HasForeignKey(userClaim => userClaim.UserId);
                c.ToTable("UserClaim");
            });
            builder.Entity<UserLogin>(c =>
            {
                c.HasOne(userLogin => userLogin.User).WithMany(user => user.Logins).HasForeignKey(userLogin => userLogin.UserId);
                c.ToTable("UserLogin");
            });
            builder.Entity<UserRole>(c =>
            {
                c.HasOne(userRole => userRole.Role).WithMany(role => role.Users).HasForeignKey(userRole => userRole.RoleId);
                c.HasOne(userRole => userRole.User).WithMany(user => user.UserRoles).HasForeignKey(userRole => userRole.UserId);
                c.ToTable("UserRole");
            });
            builder.Entity<UserToken>(c =>
            {
                c.HasOne(userToken => userToken.User).WithMany(user => user.Tokens).HasForeignKey(userToken => userToken.UserId);
                c.ToTable("UserToken");
            });
        }
    }
}