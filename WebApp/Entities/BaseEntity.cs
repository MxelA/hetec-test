using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace WebApp.Entities
{
    public class BaseEntity
    {
        [NotMapped]
        public bool SoftDelete {get; set;} = false;
        public DateTime? CreatedAt { get; set; }
        public DateTime? UpdatedAt { get; set; }
        public DateTime? DeletedAt { get; set; }
    }
}
