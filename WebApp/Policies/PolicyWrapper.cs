using Microsoft.Extensions.Configuration;
using Microsoft.AspNetCore.Http;
using WebApp.Entities.Context;
using WebApp.Repository;
using WebApp.Modules.UserManagement.Policies.User;
using WebApp.Modules.UserManagement.Policies.Permission;
using WebApp.Modules.DakarRally.Policies.VehicleModel;
using WebApp.Modules.DakarRally.Policies.Vehicle;
using WebApp.Modules.DakarRally.Policies.Race;

namespace WebApp.Policies
{
    public class PolicyWrapper: IPolicyWrapper
    {
        private ApplicationDbContext _applicationDbContext;
        private IRepositoryWrapper _repository;
        
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly IConfiguration _configuration;

        public PolicyWrapper(
            ApplicationDbContext applicationContext, 
            IConfiguration configuration, 
            IHttpContextAccessor httpContextAccessor, 
            IRepositoryWrapper repository
        ){
            _applicationDbContext   = applicationContext;
            _httpContextAccessor    = httpContextAccessor;
            _configuration          = configuration;
            _repository             = repository;
        }

        private IUserPolicy _userPolicy;
        public IUserPolicy UserPolicy {
            get {
                if(_userPolicy == null)
                {
                    _userPolicy = new UserPolicy(_repository);
                }
 
                return _userPolicy;
            }
        }
        private IPermissionPolicy _permissionPolicy;
        public IPermissionPolicy PermissionPolicy
        {
            get
            {
                if (_permissionPolicy == null)
                {
                    _permissionPolicy = new PermissionPolicy(_repository);
                }
                return _permissionPolicy;
            }
        }

        private IVehicleModelPolicy _vehicleModelPolicy;
        public IVehicleModelPolicy VehicleModelPolicy
        {
            get {
                if (_vehicleModelPolicy == null) {
                    _vehicleModelPolicy = new VehicleModelPolicy(_repository);
                }

                return _vehicleModelPolicy;
            }
        }

        private IVehiclePolicy _vehiclePolicy;
        public IVehiclePolicy VehiclePolicy
        {
            get {
                if (_vehiclePolicy == null) {
                    _vehiclePolicy = new VehiclePolicy(_repository);
                }

                return _vehiclePolicy;
            }
        }

        private IRacePolicy _racePolicy;
        public IRacePolicy RacePolicy
        {
            get {
                if (_racePolicy == null) {
                    _racePolicy = new RacePolicy(_repository);
                }

                return _racePolicy;
            }
        }
    }
}