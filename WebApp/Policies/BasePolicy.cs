using System.Collections.Generic;
using System.Linq;
using WebApp.Exceptions;
using WebApp.Modules.UserManagement.Entities;
using WebApp.Repository;

namespace WebApp.Policies
{
    public class BasePolicy: IBasePolicy
    {
        protected User AuthUser;
        protected Role AuthUserRole;
        protected ICollection<RoleClaim> AuthUserClaims;
        public readonly IRepositoryWrapper _repoistory;
        
        public BasePolicy(IRepositoryWrapper repository)
        {
            AuthUser = repository.Auth.GetAuthUser();

            if(AuthUser == null) {
                throw new HttpException(401, "Unauthorized");
            }

            AuthUserRole = AuthUser.UserRoles.First().Role;
            
            if(AuthUserRole.Claims == null)
            {
                throw new HttpException(403, "Forbidden");
            }

            AuthUserClaims = AuthUserRole.Claims;
        }
        
    }
}