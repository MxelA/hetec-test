using WebApp.Modules.DakarRally.Policies.Race;
using WebApp.Modules.DakarRally.Policies.Vehicle;
using WebApp.Modules.DakarRally.Policies.VehicleModel;
using WebApp.Modules.UserManagement.Policies.Permission;
using WebApp.Modules.UserManagement.Policies.User;

namespace WebApp.Policies
{
    public interface IPolicyWrapper
    {  
       IUserPolicy UserPolicy { get; }
       IPermissionPolicy PermissionPolicy { get; }
        IVehicleModelPolicy VehicleModelPolicy { get; }
        IVehiclePolicy VehiclePolicy { get; }
        IRacePolicy RacePolicy {get;}
    }
}