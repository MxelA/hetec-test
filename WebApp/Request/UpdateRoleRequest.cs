﻿using WebApp.Modules.UserManagement.CustomValidations;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace WebApp.Request
{
    public class UpdateRoleRequest
    {
        public string Id { get; set; }

        [Required]
        public string RoleName { get; set; }

        [PermissionValidator]
        public List<string> Permissions { get; set; }
    }
}
