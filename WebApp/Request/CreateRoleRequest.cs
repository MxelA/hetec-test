﻿using WebApp.Modules.UserManagement.CustomValidations;
using WebApp.Modules.UserManagement.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace WebApp.Request
{
    public class CreateRoleRequest
    {
        [Required]
        public string RoleName { get; set; }

        [PermissionValidator]
        public List<string> Permissions { get; set; }
    }
}
