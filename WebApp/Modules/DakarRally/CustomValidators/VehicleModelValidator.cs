﻿using WebApp.Entities.Context;
using WebApp.Repository;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using WebApp.Modules.DakarRally.Entities;

namespace WebApp.Modules.DakarRally.CustomValidators
{
    public class VehicleModelValidator : ValidationAttribute
    {

        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {

           
            var repository = (ApplicationDbContext)validationContext.GetService(typeof(ApplicationDbContext));


            VehicleModel vehicleModel = repository.VehicleModel.Where(v => v.Id.Equals(value)).FirstOrDefault();
            if (vehicleModel == null)
            {
                return new ValidationResult(GetErrorMessage());
            }

            return ValidationResult.Success;
        }

        public string GetErrorMessage()
        {
            return $"Vehicle Model not exist.";
        }
    }
}
