﻿using WebApp.Modules.DakarRally.Http.Requests;
using WebApp.Repository;
using VehicleEntity = WebApp.Modules.DakarRally.Entities.Vehicle;
using RaceEntity = WebApp.Modules.DakarRally.Entities.Race;


namespace WebApp.Modules.DakarRally.Repository.Vehicle
{
    public interface IVehicleRepository: IRepositoryBase<VehicleEntity> 
    {
        VehicleEntity CreateVehicle(CreateVehicleRequest request);
        VehicleEntity UpdateVehicle(VehicleEntity vehicle, UpdateVehicleRequest request);
        VehicleEntity AddVehicleToRace(VehicleEntity vehicle, RaceEntity race);
        VehicleEntity RemoveVehicleFromRace(VehicleEntity vehicle);
    
    }
}