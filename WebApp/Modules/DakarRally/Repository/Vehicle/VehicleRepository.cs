﻿using Microsoft.AspNetCore.Identity;
using WebApp.Repository;
using WebApp.Entities.Context;
using VehicleEntity = WebApp.Modules.DakarRally.Entities.Vehicle;
using RaceEntity = WebApp.Modules.DakarRally.Entities.Race;
using Microsoft.EntityFrameworkCore;
using WebApp.Modules.DakarRally.Http.Requests;

namespace WebApp.Modules.DakarRally.Repository.Vehicle
{
    public class VehicleRepository : RepositoryBase<VehicleEntity>, IVehicleRepository
    {
        private readonly IRepositoryWrapper repository;
        private readonly ApplicationDbContext _applicationDbContext;

        public VehicleRepository(ApplicationDbContext aplicationContext, IRepositoryWrapper repositoryWrapper)
            :base(aplicationContext)
        {
            repository          = repositoryWrapper;
            _applicationDbContext = aplicationContext;

            this.model = this.model
                .Include( v => v.VehicleModel)
                .Include(v => v.Race)
            ;
            
        }

        public VehicleEntity CreateVehicle(CreateVehicleRequest request)
        {
            VehicleEntity vehicle  = new VehicleEntity() {
              TeamName = request.TeamName ,
              ManufacturingDate = request.ManufacturingDate,
              VehicleModelId = request.VehicleModelId
            };

            Create(vehicle);
            
            return vehicle;
        } 

        public VehicleEntity UpdateVehicle(VehicleEntity vehicle, UpdateVehicleRequest request)
        {
            vehicle.TeamName            = request.TeamName;
            vehicle.ManufacturingDate   = request.ManufacturingDate;
            vehicle.VehicleModelId      = request.VehicleModelId;

            Update(vehicle);
            
            return vehicle;
        } 

        public VehicleEntity AddVehicleToRace(VehicleEntity vehicle, RaceEntity race)
        {
            vehicle.RaceId = race.Id;

            Update(vehicle);
            
            return vehicle;
        }

        public VehicleEntity RemoveVehicleFromRace(VehicleEntity vehicle)
        {
            vehicle.RaceId = null;

            Update(vehicle);
            
            return vehicle;
        }
    }
}