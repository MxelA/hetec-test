﻿using WebApp.Modules.DakarRally.Http.Requests;
using WebApp.Repository;
using VehicleRaceEntity = WebApp.Modules.DakarRally.Entities.VehicleRace;
using RaceEntity = WebApp.Modules.DakarRally.Entities.Race;
using VehicleEntity = WebApp.Modules.DakarRally.Entities.Vehicle;
using WebApp.Modules.DakarRally.Http.Resources.Vehicle;

namespace WebApp.Modules.DakarRally.Repository.VehicleRace
{
    public interface IVehicleRaceRepository: IRepositoryBase<VehicleRaceEntity> 
    {
         VehicleRaceRepository ConditionByRace(RaceEntity race);
        //  VehicleStatisticResource GetVehicleStatistic(VehicleEntity vehicle);
    }
}