﻿using WebApp.Repository;
using WebApp.Entities.Context;
using Microsoft.EntityFrameworkCore;
using VehicleRaceEntity = WebApp.Modules.DakarRally.Entities.VehicleRace;
using RaceEntity = WebApp.Modules.DakarRally.Entities.Race;
using VehicleEntity = WebApp.Modules.DakarRally.Entities.Vehicle;
using System.Linq;
using WebApp.Modules.DakarRally.Http.Resources.Vehicle;
using WebApp.Modules.DakarRally.Http.Resources;

namespace WebApp.Modules.DakarRally.Repository.VehicleRace
{
    public class VehicleRaceRepository : RepositoryBase<VehicleRaceEntity>, IVehicleRaceRepository
    {
        private readonly IRepositoryWrapper repository;
        private readonly ApplicationDbContext _applicationDbContext;

        public VehicleRaceRepository(ApplicationDbContext aplicationContext, IRepositoryWrapper repositoryWrapper)
            :base(aplicationContext)
        {
            repository          = repositoryWrapper;
            _applicationDbContext = aplicationContext;
            
        }

        public VehicleRaceRepository ConditionByRace(RaceEntity race)
        {
            this.model = this.model.Where(vr => vr.RaceId.Equals(race.Id));

            return this;
        }

        // public VehicleStatisticResource GetVehicleStatistic(VehicleEntity vehicle)
        // {
        //     var query = from vehicleRace in repository.VehicleRace.GetIQueryableModel()
        //         where vehicleRace.VehicleId.Equals(vehicle.Id)
                
        //      return query.FirstOrDefault(); 
        // }
    }
}
