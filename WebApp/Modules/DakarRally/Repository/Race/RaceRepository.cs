﻿using Microsoft.AspNetCore.Identity;
using WebApp.Repository;
using WebApp.Entities.Context;
using Microsoft.EntityFrameworkCore;
using WebApp.Modules.DakarRally.Http.Requests;
using RaceEntity = WebApp.Modules.DakarRally.Entities.Race;
using VehicleEntity = WebApp.Modules.DakarRally.Entities.Vehicle;

namespace WebApp.Modules.DakarRally.Repository.Race
{
    public class RaceRepository : RepositoryBase<RaceEntity>, IRaceRepository
    {
        private readonly IRepositoryWrapper repository;
        private readonly ApplicationDbContext _applicationDbContext;

        public RaceRepository(ApplicationDbContext aplicationContext, IRepositoryWrapper repositoryWrapper)
            :base(aplicationContext)
        {
            repository          = repositoryWrapper;
            _applicationDbContext = aplicationContext;

            // this.model = this.model
            //     .Include(r => r.Vehicles)
            //         .ThenInclude(v => v.VehicleModel)
            // ;

            
        }

        public RaceEntity CreateRace(CreateRaceRequest request)
        {
            RaceEntity race = new RaceEntity(){
                Year = request.Year,
                State = Entities.Enums.RaceStatusEnum.Pending
            };

            Create(race);
            
            return race;
        }

        public RaceEntity StartRace(RaceEntity race)
        {
            race.State = Entities.Enums.RaceStatusEnum.Running;

            Update(race);
            
            return race;
        }
    }
}

