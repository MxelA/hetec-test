﻿using WebApp.Modules.DakarRally.Http.Requests;
using WebApp.Repository;
using RaceEntity = WebApp.Modules.DakarRally.Entities.Race;
using VehicleEntity = WebApp.Modules.DakarRally.Entities.Vehicle;


namespace WebApp.Modules.DakarRally.Repository.Race
{
    public interface IRaceRepository: IRepositoryBase<RaceEntity> 
    {
        RaceEntity CreateRace(CreateRaceRequest request);
        RaceEntity StartRace(RaceEntity race);
    }
}