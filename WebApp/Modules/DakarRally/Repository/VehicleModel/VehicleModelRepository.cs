﻿using Microsoft.AspNetCore.Identity;
using WebApp.Repository;
using WebApp.Entities.Context;
using VehicleModelEntity = WebApp.Modules.DakarRally.Entities.VehicleModel;

namespace WebApp.Modules.DakarRally.Repository.VehicleModel
{
    public class VehicleModelRepository : RepositoryBase<VehicleModelEntity>, IVehicleModelRepository
    {
        private readonly IRepositoryWrapper repository;
        private readonly ApplicationDbContext _applicationDbContext;

        public VehicleModelRepository(ApplicationDbContext aplicationContext, IRepositoryWrapper repositoryWrapper)
            :base(aplicationContext)
        {
            repository          = repositoryWrapper;
            _applicationDbContext = aplicationContext;
            
        }
    }
}
