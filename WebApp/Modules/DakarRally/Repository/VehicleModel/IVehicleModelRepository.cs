﻿using WebApp.Repository;
using VehicleModelEntity = WebApp.Modules.DakarRally.Entities.VehicleModel;


namespace WebApp.Modules.DakarRally.Repository.VehicleModel
{
    public interface IVehicleModelRepository: IRepositoryBase<VehicleModelEntity> 
    {
    
    }
}