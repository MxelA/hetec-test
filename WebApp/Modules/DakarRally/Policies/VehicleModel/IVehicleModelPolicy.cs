﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApp.Modules.DakarRally.Policies.VehicleModel
{
    public interface IVehicleModelPolicy
    {
        bool CanVehicleModelView();
    }
}
