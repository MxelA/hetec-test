﻿using WebApp.Exceptions;
using WebApp.Modules.UserManagement.Entities;
using WebApp.Policies;
using WebApp.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using VehicleEntity = WebApp.Modules.DakarRally.Entities.Vehicle;

namespace WebApp.Modules.DakarRally.Policies.Vehicle
{
    public class VehiclePolicy : BasePolicy, IVehiclePolicy
    {
        private readonly IRepositoryWrapper _repository;
        public VehiclePolicy(IRepositoryWrapper repository) : base(repository)
        {
            _repository = repository;
        }


        public bool CanVehicleView()
        {
            var claim = AuthUserClaims.Where(p => p.ClaimValue.Equals(Claims.VehicleView)).FirstOrDefault();

            if (claim == null)
            {
                throw new HttpException(403, "Forbidden");
            }

            return true;
        }

        public bool CanVehicleCreate()
        {
            var claim = AuthUserClaims.Where(p => p.ClaimValue.Equals(Claims.VehicleManage)).FirstOrDefault();

            if (claim == null)
            {
                throw new HttpException(403, "Forbidden");
            }

            return true;
        }

        public bool CanVehicleUpdate(VehicleEntity vehicle)
        {
            var claim = AuthUserClaims.Where(p => p.ClaimValue.Equals(Claims.VehicleManage)).FirstOrDefault();

            if (claim == null)
            {
                throw new HttpException(403, "Forbidden");
            }

            if(vehicle.RaceId != null && vehicle.Race.State != Entities.Enums.RaceStatusEnum.Pending) {
                throw new HttpException(403, "Vehicle already in race");
            }
            return true;
        }

        public bool CanViewVehicleStatistic() 
        {
            var claim = AuthUserClaims.Where(p => p.ClaimValue.Equals(Claims.VehicleManage)).FirstOrDefault();

            if (claim == null)
            {
                throw new HttpException(403, "Forbidden");
            }

            return true;
        }
    }
}
