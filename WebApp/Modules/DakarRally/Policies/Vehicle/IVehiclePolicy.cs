﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VehicleEntity = WebApp.Modules.DakarRally.Entities.Vehicle;

namespace WebApp.Modules.DakarRally.Policies.Vehicle
{
    public interface IVehiclePolicy
    {
        bool CanVehicleView();
        bool CanVehicleCreate();
        bool CanVehicleUpdate(VehicleEntity vehicle);
        bool CanViewVehicleStatistic();
    }
}
