﻿using WebApp.Exceptions;
using WebApp.Modules.UserManagement.Entities;
using WebApp.Policies;
using WebApp.Repository;
using System.Linq;
using RaceEntity = WebApp.Modules.DakarRally.Entities.Race;
using VehicleEntity = WebApp.Modules.DakarRally.Entities.Vehicle;
using WebApp.Modules.DakarRally.Entities.Enums;

namespace WebApp.Modules.DakarRally.Policies.Race
{
    public class RacePolicy : BasePolicy, IRacePolicy
    {
        private readonly IRepositoryWrapper _repository;
        public RacePolicy(IRepositoryWrapper repository) : base(repository)
        {
            _repository = repository;
        }


        public bool CanRaceView()
        {
            var claim = AuthUserClaims.Where(p => p.ClaimValue.Equals(Claims.RaceManage)).FirstOrDefault();

            if (claim == null)
            {
                throw new HttpException(403, "Forbidden");
            }

            return true;
        }

        public bool CanRaceCreate()
        {
            var claim = AuthUserClaims.Where(p => p.ClaimValue.Equals(Claims.RaceManage)).FirstOrDefault();

            if (claim == null)
            {
                throw new HttpException(403, "Forbidden");
            }

            return true;
        }

        public bool CanAddVehicleToRace(RaceEntity race, VehicleEntity vehicle)
        {
            var claim = AuthUserClaims.Where(p => p.ClaimValue.Equals(Claims.RaceManage)).FirstOrDefault();

            if (claim == null)
            {
                throw new HttpException(403, "Forbidden");
            }

            if(race.State != RaceStatusEnum.Pending) {
                throw new HttpException(403, "Race is not in pending state");
            }

            if(vehicle.RaceId != null) {
                throw new HttpException(403, "Vehicle already in race");
            }

            return true;
        }

        public bool CanRemoveVehicleFromRace(RaceEntity race, VehicleEntity vehicle)
        {
            var claim = AuthUserClaims.Where(p => p.ClaimValue.Equals(Claims.RaceManage)).FirstOrDefault();

            if (claim == null)
            {
                throw new HttpException(403, "Forbidden");
            }

            if(race.State != RaceStatusEnum.Pending) {
                throw new HttpException(403, "Race is not in pending state");
            }

            if(vehicle.RaceId != race.Id) {
                throw new HttpException(403, "Vehicle is not in the race");
            }

            return true;
        }

        public bool CanStartRace(RaceEntity race)
        {
            var claim = AuthUserClaims.Where(p => p.ClaimValue.Equals(Claims.RaceManage)).FirstOrDefault();

            if (claim == null)
            {
                throw new HttpException(403, "Forbidden");
            }

            if(race.State != RaceStatusEnum.Pending) {
                throw new HttpException(403, "Race is race finished or running");
            }
            
            return true;
        }

        public bool CanGetRaceResult(RaceEntity race)
        {
            var claim = AuthUserClaims.Where(p => p.ClaimValue.Equals(Claims.RaceManage)).FirstOrDefault();

            if (claim == null)
            {
                throw new HttpException(403, "Forbidden");
            }

            if(race.State != RaceStatusEnum.Finished) {
                throw new HttpException(403, "Race is not in finish");
            }
            
            return true;

        }
    }
}
