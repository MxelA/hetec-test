﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using RaceEntity = WebApp.Modules.DakarRally.Entities.Race;
using VehicleEntity = WebApp.Modules.DakarRally.Entities.Vehicle;

namespace WebApp.Modules.DakarRally.Policies.Race
{
    public interface IRacePolicy
    {
        bool CanRaceView();
        bool CanRaceCreate();
        bool CanAddVehicleToRace(RaceEntity race, VehicleEntity vehicle);
        bool CanRemoveVehicleFromRace(RaceEntity race, VehicleEntity vehicle);
        bool CanStartRace(RaceEntity race);
        bool CanGetRaceResult(RaceEntity race);
    }
}
