using System;
using System.ComponentModel.DataAnnotations;
using WebApp.Modules.DakarRally.CustomValidators;

namespace WebApp.Modules.DakarRally.Http.Requests
{
    public class AddVehicleToRaceRequest
    {
        [Required]
        [RaceValidator]
        public int RaceId {get; set;}

        [Required]
        [VehicleValidator]
        public int VehicleId {get; set;}
    }
}