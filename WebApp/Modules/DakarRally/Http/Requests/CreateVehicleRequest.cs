using System;
using System.ComponentModel.DataAnnotations;
using WebApp.Modules.DakarRally.CustomValidators;

namespace WebApp.Modules.DakarRally.Http.Requests
{
    public class CreateVehicleRequest
    {
        [StringLength(100)]
        public string TeamName {get; set;}
        [Required]
        public DateTime ManufacturingDate { get; set;}
        [Required]
        [VehicleModelValidator]
        public int VehicleModelId {get; set;}
    }
}