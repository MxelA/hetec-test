using System;
using System.ComponentModel.DataAnnotations;
using WebApp.Modules.DakarRally.CustomValidators;

namespace WebApp.Modules.DakarRally.Http.Requests
{
    public class CreateRaceRequest
    {
        [Required]
        public ushort Year { get; set; }
    }
}