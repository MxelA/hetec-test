using System.Collections.Generic;
using WebApp.Modules.DakarRally.Entities;
using WebApp.Modules.DakarRally.Entities.Enums;

namespace WebApp.Modules.DakarRally.Http.Resources.Leaderboard
{
    public class LeaderboardResource
    {
        public int VehicleId {get; set;}
        public int Victory {get; set;}
        public int SpentHourInRace {get; set;}
        public int Position { get; set;}
    }
}