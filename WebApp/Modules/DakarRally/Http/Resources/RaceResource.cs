using System.Collections.Generic;
using WebApp.Modules.DakarRally.Entities;
using WebApp.Modules.DakarRally.Entities.Enums;

namespace WebApp.Modules.DakarRally.Http.Resources
{
    public class RaceResource
    {
        public int Id {get; set;}
        public ushort Year { get; set; }
        public RaceStatusEnum State {get; set;}
        public virtual ICollection<VehicleResource> Vehicles {get; set;}
    }
}