using WebApp.Modules.DakarRally.Entities;

namespace WebApp.Modules.DakarRally.Http.Resources
{
    public class VehicleResource
    {
        public int Id {get; set;}
        public string TeamName { get; set; }
        public int VehicleModelId { get; set; }
        public virtual VehicleModelResource VehicleModel {get; set;}
        public int RaceId {get; set;}
        public virtual RaceResource Race {get; set;}
    }
}