﻿using System.ComponentModel.DataAnnotations;

namespace WebApp.Modules.DakarRally.Http.Resources
{
    public class VehicleRaceResource
    {
        public int Id { get; set; }
        public int SpentHourInRace {get; set;}
        public int SpentHourMalfunctionInRace {get; set;}
        public int CurrentHourMalfunctionInRace {get; set;}
        public int CurrentDistanceInRace {get; set;}
        public bool FinishRace {get; set;}
        public bool HavyMalfunction {get; set;}
        public int? RaceId {get; set;}
        public int VehicleId {get; set;}

    }
}
