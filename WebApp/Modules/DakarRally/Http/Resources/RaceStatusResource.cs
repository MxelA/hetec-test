using System.Collections.Generic;
using System.Linq;
using WebApp.Modules.DakarRally.Entities;
using WebApp.Modules.DakarRally.Entities.Enums;
using WebApp.Repository;

namespace WebApp.Modules.DakarRally.Http.Resources
{
    public class RaceStatusResource
    {
        protected readonly IRepositoryWrapper _repository;
        public RaceStatusResource(IRepositoryWrapper repository)
        {
            _repository = repository;
        }

        public int Id {get; set;}
        public RaceStatusEnum State {get; set;}
        public IList<object> VehicleByType {
            get {
                
                return (IList<object>)_repository.Vehicle.GetNewIQueryableModel()
                    .Where(v => v.RaceId.Equals(Id))
                    .GroupBy(v => v.VehicleModelId)
                    .Select(v => new {
                        Type = v.Key,
                        Count = v.Count()
                    })
                .ToList();
            }
        }
    }
}