﻿using AutoMapper;
using WebApp.Policies;
using WebApp.Repository;
using WebApp.Request;
using WebApp.Response;
using Microsoft.AspNetCore.Mvc;
using WebApp.Modules.DakarRally.Http.Resources;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using WebApp.Modules.DakarRally.Http.Requests;
using Microsoft.AspNetCore.Authorization;
using WebApp.Modules.DakarRally.Entities;
using WebApp.Modules.DakarRally.Http.Resources.Leaderboard;
using WebApp.Modules.DakarRally.Services.LeaderboardService;
using System.Collections.Generic;
using WebApp.Modules.DakarRally.Entities.Enums;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace WebApp.Modules.DakarRally.Http.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class LeaderBoardController : ControllerBase
    {
        
        private readonly IRepositoryWrapper _repository;
        private readonly ILeaderboardService _leaderboardService;
        private readonly IMapper _mapper;
        private readonly IPolicyWrapper _policyWrapper;

        public LeaderBoardController(IRepositoryWrapper repository, IMapper mapper, IPolicyWrapper policyWrapper, ILeaderboardService leaderboardService)
        {
            _repository = repository;
            _mapper = mapper;
            _policyWrapper = policyWrapper;
            _leaderboardService = leaderboardService;
        }


        // GET: api/<PermissionController>
        [HttpGet("GetLeaderboard")]
        public IActionResult GetLeaderboard([FromQuery] PaginationParameterRequest paginationParameter)
        {
        
            return Ok(
                new ServiceResponse<PaginateListResource<LeaderboardResource>>()
                {
                    Data = _mapper.Map<PaginateListResource<LeaderboardResource>>(_leaderboardService.GetLeaderboardStatistic(paginationParameter))
                }
            );

            
        }

        [HttpGet("GetLeaderboardByVehicleType")]
        public IActionResult GetLeaderboardByVehicleType([FromQuery] PaginationParameterRequest paginationParameter, [FromQuery] VehicleModelTypeEnum typeOfVehicle)
        {
        
            return Ok(
                new ServiceResponse<PaginateListResource<LeaderboardByVehicleTypeResource>>()
                {
                    Data = _mapper.Map<PaginateListResource<LeaderboardByVehicleTypeResource>>(_leaderboardService.GetLeaderboardStatistic(paginationParameter, typeOfVehicle))
                }
            );
        }

    }
}
