﻿using AutoMapper;
using WebApp.Policies;
using WebApp.Repository;
using WebApp.Request;
using WebApp.Response;
using Microsoft.AspNetCore.Mvc;
using WebApp.Modules.DakarRally.Http.Resources;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using WebApp.Modules.DakarRally.Http.Requests;
using Microsoft.AspNetCore.Authorization;
using WebApp.Modules.DakarRally.Entities;
using WebApp.Modules.DakarRally.Services.RaceService;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace WebApp.Modules.DakarRally.Http.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class RaceController : ControllerBase
    {
        
        private readonly IRepositoryWrapper _repository;
        private readonly IMapper _mapper;
        private readonly IPolicyWrapper _policyWrapper;
        private readonly IRaceService _raceService;

        public RaceController(IRepositoryWrapper repository, IMapper mapper, IPolicyWrapper policyWrapper, IRaceService raceService)
        {
            _repository = repository;
            _mapper = mapper;
            _policyWrapper = policyWrapper;
            _raceService = raceService;
        }


        [HttpGet("GetRaces")]
        public IActionResult GetRaces([FromQuery] PaginationParameterRequest paginationParameter)
        {
            _policyWrapper.RacePolicy.CanRaceView();
            
            return Ok(
                new ServiceResponse<PaginateListResource<RaceResource>>()
                {
                    Data = _mapper.Map<PaginateListResource<RaceResource>>(
                        _repository.Race
                            .Include("Vehicles")
                            .Paginate(paginationParameter)
                    )
                }
            );
        }

        [HttpPost("CreateRace")]
        public IActionResult CreateRace([FromBody] CreateRaceRequest request)
        {
            _policyWrapper.RacePolicy.CanRaceCreate();
            
            Race race =  _repository.Race.CreateRace(request);
            _repository.Save();

            return Ok(
                new ServiceResponse<RaceResource>()
                {
                    Data = _mapper.Map<RaceResource>(race)
                }
            );
        }

        [HttpPost("AddVehicleToRace")]
        public IActionResult AddVehicleToRace([FromBody] AddVehicleToRaceRequest request)
        {
            Race race       = _repository.Race.ByCondition(r => r.Id.Equals(request.RaceId)).GetFirst();
            Vehicle vehicle = _repository.Vehicle.ByCondition(v => v.Id.Equals(request.VehicleId)).GetFirst();

            _policyWrapper.RacePolicy.CanAddVehicleToRace(race, vehicle);

            _repository.Vehicle.AddVehicleToRace(vehicle, race);
            _repository.Save();
            
            return Ok(
                new ServiceResponse<RaceResource>()
                {
                    Data = _mapper.Map<RaceResource>(race)
                }
            );
        }

         
        [HttpDelete("RemoveVehicleFromRace")]
        public IActionResult RemoveVehicleFromRace([FromBody] AddVehicleToRaceRequest request)
        {
            Race race       = _repository.Race.ByCondition(r => r.Id.Equals(request.RaceId)).GetFirst();
            Vehicle vehicle = _repository.Vehicle.ByCondition(v => v.Id.Equals(request.VehicleId)).GetFirst();

            _policyWrapper.RacePolicy.CanRemoveVehicleFromRace(race, vehicle);

            _repository.Vehicle.RemoveVehicleFromRace(vehicle);
            _repository.Save();
            
            return Ok(
                new ServiceResponse<RaceResource>()
                {
                    Data = _mapper.Map<RaceResource>(race)
                }
            );
        }

        [HttpPatch("StartRace")]
        public IActionResult StartRace([FromQuery] int raceId)
        {
            Race race = _repository.Race
                .ByCondition(r => r.Id.Equals(raceId))
                .Include("Vehicles.VehicleModel")
                .GetFirstOrFail()
            ;

            _policyWrapper.RacePolicy.CanStartRace(race);

            _raceService.StartRace(race);

            return Ok(new ServiceResponse<RaceResource>()
                {
                    Data = _mapper.Map<RaceResource>(race)
                });
        }

        [HttpGet("RaceResults")]
        public IActionResult RaceResults([FromQuery] int raceId,[FromQuery] PaginationParameterRequest paginationParameter)
        {
            Race race = _repository.Race.ByCondition(r => r.Id.Equals(raceId)).GetFirstOrFail();

            _policyWrapper.RacePolicy.CanGetRaceResult(race);

            _repository.VehicleRace.ConditionByRace(race).Paginate(paginationParameter);

            return Ok(
                new ServiceResponse<PaginateListResource<VehicleRaceResource>>()
                {
                    Data = _mapper.Map<PaginateListResource<VehicleRaceResource>>(_repository.VehicleRace.ConditionByRace(race).Paginate(paginationParameter))
                }
            );
        }

        [HttpGet("RaceStatus")]
        public IActionResult RaceStatus([FromQuery] int raceId)
        {
            _policyWrapper.RacePolicy.CanRaceView();

            Race race = _repository.Race.ByCondition(r => r.Id.Equals(raceId)).GetFirstOrFail();
            
            return Ok(
                new ServiceResponse<RaceStatusResource>()
                {
                    Data = _mapper.Map<RaceStatusResource>(race)
                }
            );
        }
    }
}
