﻿using AutoMapper;
using WebApp.Policies;
using WebApp.Repository;
using WebApp.Request;
using WebApp.Response;
using Microsoft.AspNetCore.Mvc;
using WebApp.Modules.DakarRally.Http.Resources;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using WebApp.Modules.DakarRally.Http.Requests;
using Microsoft.AspNetCore.Authorization;
using WebApp.Modules.DakarRally.Entities;
using WebApp.Modules.DakarRally.Http.Resources.Vehicle;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace WebApp.Modules.DakarRally.Http.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class VehicleController : ControllerBase
    {
        
        private readonly IRepositoryWrapper _repository;
        private readonly IMapper _mapper;
        private readonly IPolicyWrapper _policyWrapper;

        public VehicleController(IRepositoryWrapper repository, IMapper mapper, IPolicyWrapper policyWrapper)
        {
            _repository = repository;
            _mapper = mapper;
            _policyWrapper = policyWrapper;
        }


        // GET: api/<PermissionController>
        [HttpGet("GetVehicles")]
        public IActionResult GetVehicles([FromQuery] PaginationParameterRequest paginationParameter)
        {
            _policyWrapper.VehiclePolicy.CanVehicleView();
            
            return Ok(
                new ServiceResponse<PaginateListResource<VehicleResource>>()
                {
                    Data = _mapper.Map<PaginateListResource<VehicleResource>>(_repository.Vehicle.Paginate(paginationParameter))
                }
            );
        }

        [HttpPost("CreateVehicle")]
        public IActionResult CreateVehicle([FromBody] CreateVehicleRequest createVehicleRequest)
        {
            _policyWrapper.VehiclePolicy.CanVehicleCreate();
            
            Vehicle  vehicle = _repository.Vehicle.CreateVehicle(createVehicleRequest);
            _repository.Save();

            return Ok(
                new ServiceResponse<VehicleResource>()
                {
                    Data = _mapper.Map<VehicleResource>(vehicle)
                }
            );
        }

        [HttpPut("UpdateVehicle")]
        public IActionResult UpdateVehicle([FromBody] UpdateVehicleRequest updateVehicleRequest)
        {
            Vehicle vehicle = _repository.Vehicle.ByCondition(v => v.Id.Equals(updateVehicleRequest.Id)).GetFirst();

            _policyWrapper.VehiclePolicy.CanVehicleUpdate(vehicle);
            
            vehicle = _repository.Vehicle.UpdateVehicle(vehicle, updateVehicleRequest);
            _repository.Save();

            return Ok(
                new ServiceResponse<VehicleResource>()
                {
                    Data = _mapper.Map<VehicleResource>(vehicle)
                }
            );
        }


        [HttpGet("VehicleStatistic")]
        public IActionResult VehicleStatistic([FromQuery] int vehicleId, [FromQuery] PaginationParameterRequest paginationParamete)
        {
            _policyWrapper.VehiclePolicy.CanViewVehicleStatistic();

            Vehicle vehicle = _repository.Vehicle.ByCondition(v => v.Id.Equals(vehicleId)).GetFirstOrFail();   
          
            return Ok(
                new ServiceResponse<PaginateListResource<VehicleRaceResource>>()
                {
                    Data =  _mapper.Map<PaginateListResource<VehicleRaceResource>>(_repository.VehicleRace.ByCondition(vr => vr.VehicleId.Equals(vehicleId)).Paginate(paginationParamete))
                }
            );
        }
       

    }
}
