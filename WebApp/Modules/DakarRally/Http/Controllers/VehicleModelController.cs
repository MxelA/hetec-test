﻿using AutoMapper;
using WebApp.Modules.UserManagement.Entities;
using WebApp.Policies;
using WebApp.Repository;
using WebApp.Request;
using WebApp.Response;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using WebApp.Modules.DakarRally.Http.Resources;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace WebApp.Modules.DakarRally.Http.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    // [Authorize]
    public class VehicleModelController : ControllerBase
    {
        
        private readonly IRepositoryWrapper _repository;
        private readonly IMapper _mapper;
        private readonly IPolicyWrapper _policyWrapper;

        public VehicleModelController(IRepositoryWrapper repository, IMapper mapper, IPolicyWrapper policyWrapper)
        {
            _repository = repository;
            _mapper = mapper;
            _policyWrapper = policyWrapper;
        }


        // GET: api/<PermissionController>
        [HttpGet("GetVehicleModels")]
        public IActionResult GetVehicleModels([FromQuery] PaginationParameterRequest paginationParameter)
        {
            _policyWrapper.VehicleModelPolicy.CanVehicleModelView();
            
            return Ok(
                new ServiceResponse<PaginateListResource<VehicleModelResource>>()
                {
                    Data = _mapper.Map<PaginateListResource<VehicleModelResource>>(_repository.VehicleModel.Paginate(paginationParameter))
                }
            );
        }



       

    }
}
