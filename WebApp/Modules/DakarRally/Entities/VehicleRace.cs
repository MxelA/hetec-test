﻿using WebApp.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace WebApp.Modules.DakarRally.Entities
{
    public class VehicleRace : BaseEntity
    {
        
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        [StringLength(100)]
        public int SpentHourInRace {get; set;}
        public int SpentHourMalfunctionInRace {get; set;}
        public int CurrentHourMalfunctionInRace {get; set;}
        public int CurrentDistanceInRace {get; set;}
        public bool FinishRace {get; set;}
        public bool HavyMalfunction {get; set;}
        public int RaceId {get; set;}
        public virtual Race Race{ get; set;}
        public int VehicleId {get; set;}
        public virtual Vehicle Vehicle {get; set;}

    }
}
