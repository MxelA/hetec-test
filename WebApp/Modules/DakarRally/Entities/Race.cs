﻿using WebApp.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;
using WebApp.Modules.DakarRally.Entities.Enums;

namespace WebApp.Modules.DakarRally.Entities
{
    public class Race : BaseEntity
    {
        
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        [Required]
        public ushort Year { get; set; }

        public RaceStatusEnum State {get; set;}
        #nullable enable
        public virtual ICollection<Vehicle>? Vehicles {get; set;}
        public virtual ICollection<VehicleRace>? VehicleRaces {get; set;}
    }
}
