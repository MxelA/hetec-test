﻿using WebApp.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;
using WebApp.Modules.DakarRally.Entities.Enums;

namespace WebApp.Modules.DakarRally.Entities
{
    public class VehicleModel : BaseEntity
    {
        
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        [StringLength(50)]
        public string Name { get; set; }
        public int Speed {get; set;}
        public int RepairTime { get; set;}
        public int ProbabilityLightMalfunctions { get; set; }
        public int ProbabilityHeavyMalfunctions { get; set; }
        public VehicleModelTypeEnum? ModelType {get; set;}

    }
}
