﻿using WebApp.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace WebApp.Modules.DakarRally.Entities
{
    public class Vehicle : BaseEntity
    {
        
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        [StringLength(100)]
        public string TeamName { get; set; }
        public DateTime ManufacturingDate { get; set;}
        public int VehicleModelId {get; set;}
        public virtual VehicleModel VehicleModel {get; set;}
        public int? RaceId {get; set;}
        public virtual Race Race{ get; set;}

    }
}
