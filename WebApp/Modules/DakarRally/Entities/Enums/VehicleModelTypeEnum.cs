using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace WebApp.Modules.DakarRally.Entities.Enums
{
    [JsonConverter(typeof(StringEnumConverter))]
    public enum VehicleModelTypeEnum
    {
            Car = 1,
            Truck,
            Motorcycle
    }
}