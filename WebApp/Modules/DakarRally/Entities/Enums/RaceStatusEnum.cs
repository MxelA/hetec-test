namespace WebApp.Modules.DakarRally.Entities.Enums
{
    public enum RaceStatusEnum
    {
            Pending = 1,
            Running,
            Finished
    }
}