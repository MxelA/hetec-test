
using AutoMapper;
using WebApp.Response;
using WebApp.Modules.UserManagement.Entities;
using WebApp.Modules.UserManagement.Http.Resources;
using WebApp.Modules.DakarRally.Entities;
using WebApp.Modules.DakarRally.Http.Resources;
using WebApp.Modules.DakarRally.Http.Resources.Leaderboard;

namespace WebApp.Modules.DakarRally.Mapper
{
    public class DakarRallyMappingProfile : Profile
    {
        public DakarRallyMappingProfile()
        {
            // Add as many of these lines as you need to map your objects
            CreateMap<VehicleModel, VehicleModelResource>();
            CreateMap<ServicePaginateList<VehicleModel>, PaginateListResource<VehicleModelResource>>();

            CreateMap<Vehicle, VehicleResource>();
            CreateMap<ServicePaginateList<Vehicle>, PaginateListResource<VehicleResource>>();

            CreateMap<Race, RaceResource>();
            CreateMap<Race, RaceStatusResource>();
            CreateMap<ServicePaginateList<Race>, PaginateListResource<RaceResource>>();

            CreateMap<VehicleRace, VehicleRaceResource>();
            CreateMap<ServicePaginateList<VehicleRace>, PaginateListResource<VehicleRaceResource>>();

            CreateMap<ServicePaginateList<LeaderboardResource>, PaginateListResource<LeaderboardResource>>();
            CreateMap<ServicePaginateList<LeaderboardByVehicleTypeResource>, PaginateListResource<LeaderboardByVehicleTypeResource>>();

            
        }
    }
}