using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using WebApp.Exceptions;
using WebApp.Modules.DakarRally.Entities;
using WebApp.Modules.DakarRally.Http.Resources.Leaderboard;
using WebApp.Repository;

namespace WebApp.Modules.DakarRally.Services.RaceService
{
    public class RaceService: IRaceService
    {
        protected readonly IRepositoryWrapper _repository;
        protected Dictionary<int, VehicleRace> _raceStats = new Dictionary<int, VehicleRace>();
        protected bool _endRace = false;
        protected Random randomGenerator = new Random();

        public RaceService(IRepositoryWrapper repository)
        {
            _repository = repository;
        }

        public Race StartRace(Race race)
        {
            if(race.State != Entities.Enums.RaceStatusEnum.Pending) {
                throw new HttpException(403, "Race is not in panding state");
            }

            if(race.Vehicles.Count() == 0) {
                throw new HttpException(403, "No vehicles in race");
            }
            
            _repository.Race.StartRace(race);
            _repository.Save();
            IList<Vehicle> vehicleInRace = new List<Vehicle>(race.Vehicles.ToArray());
            
            while (!_endRace) 
            {
                foreach(var vehicle in race.Vehicles)
                {
                    var isVehicleInRace = vehicleInRace.Where(v => v.Id.Equals(vehicle.Id)).FirstOrDefault();
                    
                    if(isVehicleInRace == null) {
                        continue;
                    }

                    var driving = DrivingVehicle(vehicle, race);
                    
                    if(!driving) {
                        vehicleInRace.Remove(vehicle);
                    }

                    _repository.Save();
                }

                if(vehicleInRace.Count() == 0) {
                    _endRace = true;
                }
            }
            
            race.Vehicles.ToList().ForEach(v => v.RaceId = null);
            race.State = Entities.Enums.RaceStatusEnum.Finished;
            _repository.Race.Update(race);
            _repository.Save();

            return race;
        }

        private bool DrivingVehicle(Vehicle vehicle, Race race)
        {

            if(!_raceStats.ContainsKey(vehicle.Id))
            {
                _raceStats.Add(vehicle.Id, new VehicleRace(){
                    VehicleId = vehicle.Id,
                    RaceId  = race.Id
                });

                _repository.VehicleRace.Create(_raceStats[vehicle.Id]);
                _repository.Save();
            }

            if(_raceStats[vehicle.Id].CurrentHourMalfunctionInRace > 0)
            {
                _raceStats[vehicle.Id].CurrentHourMalfunctionInRace--;
                _raceStats[vehicle.Id].SpentHourMalfunctionInRace++;
                _raceStats[vehicle.Id].SpentHourInRace++;

                _repository.VehicleRace.Update(_raceStats[vehicle.Id]);
                return true;
            }

            switch(MalfunctionVehicle(vehicle)) 
            {
                case 1: //Light malfunction
                    _raceStats[vehicle.Id].CurrentHourMalfunctionInRace = vehicle.VehicleModel.RepairTime;
                    _repository.VehicleRace.Update(_raceStats[vehicle.Id]);
                    return true;

                case 2://havy malfunction
                    _raceStats[vehicle.Id].HavyMalfunction = true;
                    _repository.VehicleRace.Update(_raceStats[vehicle.Id]);
                    return false;

                default:
                    _raceStats[vehicle.Id].SpentHourInRace++;
                    _raceStats[vehicle.Id].CurrentDistanceInRace += vehicle.VehicleModel.Speed;
                    
                    if(_raceStats[vehicle.Id].CurrentDistanceInRace >= 10000)
                    {
                        _raceStats[vehicle.Id].FinishRace = true;
                        _repository.VehicleRace.Update(_raceStats[vehicle.Id]);
                        return false;
                    }

                    _repository.VehicleRace.Update(_raceStats[vehicle.Id]);
                    return true;
                       
            }
        }

        private int MalfunctionVehicle(Vehicle vehicle)
        {
            var ProbabilityLightMalfunctions = vehicle.VehicleModel.ProbabilityLightMalfunctions;
            var ProbabilityHeavyMalfunctions = vehicle.VehicleModel.ProbabilityHeavyMalfunctions;

            var probabilityMalfunction = randomGenerator.Next(randomGenerator.Next(0,ProbabilityHeavyMalfunctions),100);
            
            if(probabilityMalfunction == ProbabilityHeavyMalfunctions)
            {
                return 2;
            }

            if(probabilityMalfunction > ProbabilityHeavyMalfunctions && probabilityMalfunction <= (ProbabilityLightMalfunctions + ProbabilityHeavyMalfunctions) ) 
            {
                return 1;
            }
            
            return 0;
            
        }
    }
}