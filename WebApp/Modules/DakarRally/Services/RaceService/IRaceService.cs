using WebApp.Modules.DakarRally.Entities;

namespace WebApp.Modules.DakarRally.Services.RaceService
{
    public interface IRaceService
    {
        Race StartRace(Race race);
    }
}