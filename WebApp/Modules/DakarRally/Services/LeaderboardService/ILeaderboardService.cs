using System.Collections.Generic;
using System.Linq;
using WebApp.Modules.DakarRally.Entities.Enums;
using WebApp.Modules.DakarRally.Http.Resources.Leaderboard;
using WebApp.Request;
using WebApp.Response;

namespace WebApp.Modules.DakarRally.Services.LeaderboardService
{
    public interface ILeaderboardService
    {        
        ServicePaginateList<LeaderboardResource> GetLeaderboardStatistic(PaginationParameterRequest paginationParameter);
        ServicePaginateList<LeaderboardByVehicleTypeResource> GetLeaderboardStatistic(PaginationParameterRequest paginationParameter, VehicleModelTypeEnum vehicleModelType);
    }
}