using System.Linq;
using Microsoft.EntityFrameworkCore;
using WebApp.Entities.Context;
using WebApp.Modules.DakarRally.Entities.Enums;
using WebApp.Modules.DakarRally.Http.Resources.Leaderboard;
using WebApp.Repository;
using WebApp.Request;
using WebApp.Response;

namespace WebApp.Modules.DakarRally.Services.LeaderboardService
{
    public class LeaderboardService: ILeaderboardService
    {
        protected readonly IRepositoryWrapper _repository;
        protected readonly ApplicationDbContext _db;
       
        public LeaderboardService(IRepositoryWrapper repository, ApplicationDbContext db)
        {
            _repository = repository;
            _db = db;
        }

        public ServicePaginateList<LeaderboardResource> GetLeaderboardStatistic(PaginationParameterRequest paginationParameter)
        {
        
            var query = from vehicleRace in _repository.VehicleRace.GetIQueryableModel()
                group vehicleRace by new {
                    vehicleRace.VehicleId,
                    vehicleRace.FinishRace
                } into vehicleRaceGroup
                orderby vehicleRaceGroup.Where(vr => vr.FinishRace.Equals(true)).Sum(vr => vr.SpentHourInRace) ascending
                orderby vehicleRaceGroup.Where(vr => vr.FinishRace.Equals(true)).Count(vr => vr.FinishRace) descending
                select( new LeaderboardResource(){
                    VehicleId = vehicleRaceGroup.Key.VehicleId,
                    Victory = vehicleRaceGroup.Where(v => v.FinishRace.Equals(true)).Count(),
                    SpentHourInRace = vehicleRaceGroup.Where(vr => vr.FinishRace.Equals(true)).Sum(v => v.SpentHourInRace)
                });

            var list = ServicePaginateList<LeaderboardResource>.ToPaginateList(query, paginationParameter); 

            list.Items = list.Items.Select((r, index) => new LeaderboardResource{
                VehicleId = r.VehicleId,
                Victory = r.Victory,
                SpentHourInRace = r.SpentHourInRace,
                Position = ((paginationParameter.PageNumber - 1) * paginationParameter.PageSize) + index + 1 
            }).ToList();

            return list;
        }

        public ServicePaginateList<LeaderboardByVehicleTypeResource> GetLeaderboardStatistic(PaginationParameterRequest paginationParameter, VehicleModelTypeEnum vehicleModelType)
        {
        
            var query = from vehicleRace in _repository.VehicleRace.GetIQueryableModel()
                from vehicle in  _repository.Vehicle.GetIQueryableModel()
                from vehicleModel in _repository.VehicleModel.GetIQueryableModel()
                where vehicle.Id == vehicleRace.VehicleId
                where vehicleModel.Id == vehicle.VehicleModelId
                where vehicleModel.ModelType.Equals(vehicleModelType) 
                group vehicleRace by new {
                    vehicleModel.ModelType
                } into vehicleRaceGroup
                orderby vehicleRaceGroup.Where(v => v.FinishRace.Equals(true)).Count() descending
                orderby vehicleRaceGroup.Where(vr => vr.FinishRace.Equals(true)).Count(vr => vr.FinishRace) descending
                select( new LeaderboardByVehicleTypeResource(){
                    VehicleType = vehicleRaceGroup.Key.ModelType,
                    Victory = vehicleRaceGroup.Where(v => v.FinishRace.Equals(true)).Count(),
                    SpentHourInRace = vehicleRaceGroup.Where(v => v.FinishRace.Equals(true)).Sum(v => v.SpentHourInRace)
                });
                
           var list = ServicePaginateList<LeaderboardByVehicleTypeResource>.ToPaginateList(query, paginationParameter); 

            list.Items = list.Items.Select((r, index) => new LeaderboardByVehicleTypeResource{
                VehicleType = r.VehicleType,
                Victory = r.Victory,
                SpentHourInRace = r.SpentHourInRace,
                Position = ((paginationParameter.PageNumber - 1) * paginationParameter.PageSize) + index + 1 
            }).ToList();

            return list;
        }
    }
}