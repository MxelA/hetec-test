using WebApp.Entities.Context;
using WebApp.Modules.DakarRally.Entities;

namespace WebApp.Modules.DakarRally.Seeders
{
    public static class VehicleModelSeeder
    {
       public static void Seed(ApplicationDbContext context)
        {

            context.VehicleModel.AddRange(new[]{
                new VehicleModel(){
                    Id = 1,
                    Name = "Sport Car",
                    ModelType = Entities.Enums.VehicleModelTypeEnum.Car,
                    Speed = 140,
                    RepairTime = 5,
                    ProbabilityLightMalfunctions = 12,
                    ProbabilityHeavyMalfunctions = 2
                    
                },
                new VehicleModel(){
                    Id = 2,
                    Name = "Terrain Car",
                    ModelType = Entities.Enums.VehicleModelTypeEnum.Car,
                    Speed = 100,
                    RepairTime = 5,
                    ProbabilityLightMalfunctions = 3,
                    ProbabilityHeavyMalfunctions = 1
                },
                new VehicleModel(){
                    Id = 3,
                    Name = "Truck",
                    ModelType = Entities.Enums.VehicleModelTypeEnum.Truck,
                    Speed = 80,
                    RepairTime = 7,
                    ProbabilityLightMalfunctions = 6,
                    ProbabilityHeavyMalfunctions = 4
                },
                new VehicleModel(){
                    Id = 4,
                    Name = "Motorcycle Cross",
                    ModelType = Entities.Enums.VehicleModelTypeEnum.Motorcycle,
                    Speed = 85,
                    RepairTime = 3,
                    ProbabilityLightMalfunctions = 3,
                    ProbabilityHeavyMalfunctions = 2
                },
                new VehicleModel(){
                    Id = 5,
                    Name = "Motorcycle Sport",
                    ModelType = Entities.Enums.VehicleModelTypeEnum.Motorcycle,
                    Speed = 130,
                    RepairTime = 3,
                    ProbabilityLightMalfunctions = 18,
                    ProbabilityHeavyMalfunctions = 10
                },
            });

            context.SaveChanges();
        }
 
    }
}