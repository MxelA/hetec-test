using System;
using WebApp.Entities.Context;
using WebApp.Modules.DakarRally.Entities;
using WebApp.Modules.DakarRally.Entities.Enums;

namespace WebApp.Modules.DakarRally.Seeders
{
    public static class RaceSeeder
    {
       public static void Seed(ApplicationDbContext context)
        {

            context.Race.AddRange(new[]{
                new Race(){
                    Id = 1,
                    Year = 2021,
                    State = RaceStatusEnum.Pending
                }
            });

            context.SaveChanges();
        }
 
    }
}