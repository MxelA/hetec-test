using System;
using WebApp.Entities.Context;
using WebApp.Modules.DakarRally.Entities;

namespace WebApp.Modules.DakarRally.Seeders
{
    public static class VehicleSeeder
    {
       public static void Seed(ApplicationDbContext context)
        {

            context.Vehicle.AddRange(new[]{
                new Vehicle(){
                    Id = 1,
                    TeamName = "Team 1",
                    ManufacturingDate = DateTime.Parse("2018-01-01"),
                    VehicleModelId = 1,
                    RaceId = 1
                },
                new Vehicle(){
                    Id = 2,
                    TeamName = "Team 2",
                    ManufacturingDate = DateTime.Parse("2018-01-01"),
                    VehicleModelId = 2,
                    RaceId = 1
                }
            });

            context.SaveChanges();
        }
 
    }
}