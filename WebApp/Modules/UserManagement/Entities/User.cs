using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.AspNetCore.Identity;

namespace WebApp.Modules.UserManagement.Entities
{
   public class User : IdentityUser<int>
    {
        [PersonalData]
        [StringLength(50)]
        public string FirstName { get; set; }

        [PersonalData]
        [StringLength(50)]
        public string LastName { get; set; }
        public DateTime? CreatedAt { get; set; }
        public DateTime? UpdatedAt { get; set; }
        
        public DateTime? DeletedAt { get; set; }
        [NotMapped]
        public bool SoftDelete { get; set; } = true;
        public virtual ICollection<UserRole> UserRoles {get; set;}
        public virtual ICollection<UserClaim> Claims {get; set;}
        public virtual ICollection<UserToken> Tokens {get; set;}
        public virtual ICollection<UserLogin> Logins {get; set;}
    }
}