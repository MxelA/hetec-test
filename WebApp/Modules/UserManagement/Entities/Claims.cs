﻿using WebApp.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace WebApp.Modules.UserManagement.Entities
{
    public class Claims : BaseEntity
    {
        #region Permission
        public static string PermissionView = "Permission.View";
        #endregion

        #region Role
        public static string RoleView = "Role.View";
        public static string RoleManage = "Role.Manage";
        #endregion

        #region User
        public static string UserView = "User.View";
        public static string UserManage = "User.Manage";
        #endregion

        #region DakarRelly
        public static string VehicleModelView = "VehicleModel.View";
        public static string VehicleView = "Vehicle.View";
        public static string VehicleManage = "Vehicle.Manage";
        public static string RaceView = "Race.View";
        public static string RaceManage = "Race.Manage";
        #endregion


        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public string Name { get; set; }


    }
}
