using Microsoft.AspNetCore.Identity;
namespace WebApp.Modules.UserManagement.Entities
{
    public class UserLogin: IdentityUserLogin<int>
    {
        public virtual User User { get; set; }
    }
}