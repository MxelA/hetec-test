using System.Collections.Generic;
using Microsoft.AspNetCore.Identity;

namespace WebApp.Modules.UserManagement.Entities
{
    public class Role : IdentityRole<int>
    { 
        public virtual ICollection<UserRole> Users {get; set;}
        public virtual ICollection<RoleClaim> Claims {get; set;}
    }
}