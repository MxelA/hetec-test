using Microsoft.AspNetCore.Identity;
namespace WebApp.Modules.UserManagement.Entities
{
    public class UserClaim: IdentityUserClaim<int>
    {
       public virtual User User { get; set; } 
    }
}