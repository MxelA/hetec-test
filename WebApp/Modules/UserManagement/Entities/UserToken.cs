using Microsoft.AspNetCore.Identity;
namespace WebApp.Modules.UserManagement.Entities
{
    public class UserToken: IdentityUserToken<int>
    {
        public virtual User User { get; set; }
    }
}