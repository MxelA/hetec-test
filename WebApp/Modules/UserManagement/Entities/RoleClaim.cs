using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Identity;
namespace WebApp.Modules.UserManagement.Entities
{
    public class RoleClaim: IdentityRoleClaim<int>
    {
         public virtual Role Role { get; set; }
    }
}