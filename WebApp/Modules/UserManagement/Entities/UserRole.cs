using Microsoft.AspNetCore.Identity;
namespace WebApp.Modules.UserManagement.Entities
{
    public class UserRole: IdentityUserRole<int>
    {
        public virtual User User { get; set; }
        public virtual Role Role { get; set; }
    }
}