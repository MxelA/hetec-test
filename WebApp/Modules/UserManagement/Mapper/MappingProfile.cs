
using AutoMapper;
using WebApp.Response;
using WebApp.Modules.UserManagement.Entities;
using WebApp.Modules.UserManagement.Http.Resources;

namespace WebApp.Modules.UserManagement.Mapper
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            // Add as many of these lines as you need to map your objects
            CreateMap<User, UserResource>();
            CreateMap<ServicePaginateList<User>, PaginateListResource<UserResource>>();
            CreateMap<Claims, ClaimsResource>();
            CreateMap<ServicePaginateList<Claims>, PaginateListResource<ClaimsResource>>();
            CreateMap<Role, RoleResource>();
            CreateMap<ServicePaginateList<Role>, PaginateListResource<RoleResource>>();
        }
    }
}