﻿using WebApp.Entities.Context;
using WebApp.Repository;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace WebApp.Modules.UserManagement.CustomValidations
{
    public class PermissionValidator : ValidationAttribute
    {

        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {

            IList<string> collection = (IList<string>)value;
            var repository = (ApplicationDbContext)validationContext.GetService(typeof(ApplicationDbContext));


            var claim = repository.Claims.Where(u => collection.Contains(u.Name)).ToList();
            if (claim.Count != collection.Count)
            {
                return new ValidationResult(GetErrorMessage());
            }

            return ValidationResult.Success;
        }

        public string GetErrorMessage()
        {
            return $"Unijeli ste rolu koja ne postoji.";
        }
    }
}
