﻿
using WebApp.Modules.UserManagement.Entities;
using WebApp.Repository;
namespace WebApp.Modules.UserManagement.Repository.PermissionRepo
{
   public interface IPermissionRepository : IRepositoryBase<Claims>
    {
    }
}
