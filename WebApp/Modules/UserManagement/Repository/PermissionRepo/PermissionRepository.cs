﻿using WebApp.Entities;
using WebApp.Entities.Context;
using WebApp.Repository;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using System;
using UserEntity = WebApp.Modules.UserManagement.Entities.User;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using WebApp.Modules.UserManagement.Entities;

namespace WebApp.Modules.UserManagement.Repository.PermissionRepo
{
    public class PermissionRepository : RepositoryBase<Claims>, IPermissionRepository
    {
        private readonly IConfiguration _configuration;
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly IRepositoryWrapper repository;
        private readonly SignInManager<UserEntity> _signInManager;
        private readonly UserManager<UserEntity> _userManager;
        private readonly ApplicationDbContext _applicationContext;
        public PermissionRepository(
            ApplicationDbContext aplicationContext,
            IRepositoryWrapper repositoryWrapper,
            IConfiguration configuration,
            IHttpContextAccessor httpContextAccessor,
            SignInManager<UserEntity> signInManager,
            UserManager<UserEntity> userManager

        )
            : base(aplicationContext)
        {
            repository = repositoryWrapper;
            _configuration = configuration;
            _httpContextAccessor = httpContextAccessor;
            _signInManager = signInManager;
            _userManager = userManager;
            _applicationContext = aplicationContext;
        }
    }
}
