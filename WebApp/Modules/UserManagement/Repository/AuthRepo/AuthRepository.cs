using Microsoft.Extensions.Configuration;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.IdentityModel.Tokens;

using System;
using System.Text;
using System.Threading.Tasks;
using WebApp.Repository;
using UserEntity = WebApp.Modules.UserManagement.Entities.User;
using WebApp.Entities.Context;
using WebApp.Modules.UserManagement.Http.Requests;
using System.Collections.Generic;
using System.Security.Claims;
using System.IdentityModel.Tokens.Jwt;
using WebApp.Exceptions;
using WebApp.Response;
using WebApp.Modules.UserManagement.Http.Resources;
using System.Linq;
using Microsoft.EntityFrameworkCore;

namespace WebApp.Modules.UserManagement.Repository.Auth
{
    public class AuthRepository: RepositoryBase<UserEntity>, IAuthRepository
    {
        private readonly IConfiguration _configuration;
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly IRepositoryWrapper repository;
        private readonly SignInManager<UserEntity> _signInManager;
        private readonly UserManager<UserEntity> _userManager;
        private readonly ApplicationDbContext _applicationContext;
        public AuthRepository(
            ApplicationDbContext aplicationContext, 
            IRepositoryWrapper repositoryWrapper, 
            IConfiguration configuration, 
            IHttpContextAccessor httpContextAccessor,
            SignInManager<UserEntity> signInManager,
            UserManager<UserEntity> userManager
            
        )
            :base(aplicationContext)
        {
            repository              = repositoryWrapper;
            _configuration          = configuration;
            _httpContextAccessor    = httpContextAccessor;
            _signInManager          = signInManager;
            _userManager            = userManager;
            _applicationContext     = aplicationContext;
        }

        public async Task<ServiceResponse<LoginResource>> GetUserJwt(LoginRequest loginRequest)
        {
            UserEntity user = await repository.User
                .ByCondition(u => (u.Email.Equals(loginRequest.Email)))
                .GetFirstAsync()
            ;

            var result = await _signInManager.PasswordSignInAsync(loginRequest.Email, loginRequest.Password, false, false);

            if (user == null || !result.Succeeded) {
                 throw new HttpException(401,"Prijava nije uspjela. Provjerite vas e-mail i lozinku pa pokušajte ponovo.");
            }

            return  new ServiceResponse<LoginResource>() {
                Data = new LoginResource() {
                    FirstName = user.FirstName,
                    LastName = user.LastName,
                    Token    = CreateJwtToken(user)
                }
            };
        }

        

        private string CreateJwtToken(UserEntity user)
        {
            var claims = new List<Claim> {
                new Claim(ClaimTypes.NameIdentifier, user.Id.ToString()),
                new Claim(ClaimTypes.Name, user.FirstName + " " + user.LastName)
            };

            SymmetricSecurityKey key = new SymmetricSecurityKey(
                Encoding.UTF8.GetBytes(_configuration.GetSection("Jwt:Secret").Value)
            );

            SigningCredentials creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha512Signature);

            SecurityTokenDescriptor tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(claims),
                Expires = DateTime.Now.AddMinutes(int.Parse(_configuration.GetSection("Jwt:Expire").Value)),
                SigningCredentials = creds
            };

            JwtSecurityTokenHandler tokenHandler = new JwtSecurityTokenHandler();
            SecurityToken token = tokenHandler.CreateToken(tokenDescriptor);
            
            return tokenHandler.WriteToken(token);
        }

        // public async Task<User> GetAuthUserAsync()
        // {
        //     var a = _httpContextAccessor.HttpContext.User.Claims;
        //     string authUserId = _httpContextAccessor.HttpContext.User.Claims.Single(x => x.Type == ClaimTypes.NameIdentifier).Value;
        //     return await FindByCondition( u => u.Id.Equals(authUserId)).FirstOrDefaultAsync(); 
        // }

        public UserEntity GetAuthUser() 
        {
            var userClaimId = _httpContextAccessor.HttpContext.User.Claims.Where(x => x.Type == ClaimTypes.NameIdentifier).SingleOrDefault();

            if(userClaimId == null)
            {
                return null;
            }

            return ByCondition( u => u.Id.Equals(Int32.Parse(userClaimId.Value)))
                .GetIQueryableModel()
                .Include(u => u.UserRoles)
                    .ThenInclude(r => r.Role)
                        .ThenInclude(c => c.Claims)
                .FirstOrDefault()
            ;  
        }       
    }
}