using System.Threading.Tasks;
using WebApp.Response;
using WebApp.Modules.UserManagement.Http.Requests;
using WebApp.Modules.UserManagement.Http.Resources;
using WebApp.Repository;
using UserEntity = WebApp.Modules.UserManagement.Entities.User;

namespace WebApp.Modules.UserManagement.Repository.Auth
{
    public interface IAuthRepository : IRepositoryBase<UserEntity>
    {
        Task<ServiceResponse<LoginResource>> GetUserJwt(LoginRequest loginRequest);
        // Task<User> GetAuthUserAsync();
        UserEntity GetAuthUser();

        //  Task<string> LoginTempToken(string jwt);
        // void CreatePasswordHash(string password, out byte[] passwordHash, out byte[] passwordSalt);
        // int GetAuthUserId();
    }
}