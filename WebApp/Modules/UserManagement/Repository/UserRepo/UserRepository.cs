﻿using Microsoft.AspNetCore.Identity;
using WebApp.Repository;
using UserEnity = WebApp.Modules.UserManagement.Entities.User;
using WebApp.Entities.Context;
using System.Linq;
using System;
using WebApp.Modules.UserManagement.Http.Requests;
using System.Threading.Tasks;
using System.Collections;
using System.Collections.Generic;

namespace WebApp.Modules.UserManagement.Repository.User
{
    public class UserRepository : RepositoryBase<UserEnity>, IUserRepository
    {
        private readonly IRepositoryWrapper repository;
         private readonly UserManager<UserEnity> _userManager;
        private readonly ApplicationDbContext _applicationDbContext;

        public UserRepository(ApplicationDbContext aplicationContext, IRepositoryWrapper repositoryWrapper, UserManager<UserEnity> userManager)
            :base(aplicationContext)
        {
            repository          = repositoryWrapper;
            _applicationDbContext = aplicationContext;
             _userManager        = userManager;
            
        }

        public async Task<UserEnity> CreateUser(UserCreateRequest request)
        {
           
            if(request.Password != request.ConfirmPassword)
            {
                throw new Exception("Sifre se ne podudaraju!");
            }

            var userExist = await _userManager.FindByEmailAsync(request.Email);

            if (userExist != null)
            {
                throw new Exception("Korisnik vec postoji");
            }

            //Check role
            var roleExist = DoesRoleExist(request.RoleName);

            if (!roleExist)
            {
                throw new Exception("Rola koju ste unijeli ne postoji!");
            }

            var user = new UserEnity()
            {
                FirstName = request.FirstName,
                LastName = request.LastName,
                Email = request.Email,
                UserName = request.Email,
                EmailConfirmed = true
            };
            
            var result = await _userManager.CreateAsync(user, request.Password);

            var assignRole = await _userManager.AddToRoleAsync(user, request.RoleName);    

            _applicationDbContext.SaveChanges();

            return user;
        }

        public UserRepository CriteriaByName(string name)
        {
            if(!string.IsNullOrEmpty(name)) {
                this.model = model.Where(u => u.FirstName == name);    
            }
            
            return this;
        }


        public UserEnity GetUserById(int id)
        {

          var user = ByCondition(u => u.Id.Equals(id))
                .GetIQueryableModel().FirstOrDefault();
            ;

            return user;
        }

        public async Task<UserEnity> UpdateUser(int id, UserUpdateRequest request)
        {
            var user = GetUserById(id);
            if(user == null)
            {
                throw new Exception("Korisnik nije pronadjen");
            }

            if (user.Email != request.Email)
            {
                var userExist = await _userManager.FindByEmailAsync(request.Email);
                if (userExist != null)
                {
                    throw new Exception("Email je vec registrovan u sistemu");
                }
            }
            //UPDATE USER
            user.FirstName = request.FirstName;
            user.LastName = request.LastName;
            user.Email = request.Email;
            user.NormalizedUserName = request.Email;
            user.UserName = request.Email;
            await _userManager.UpdateAsync(user);

            //UPDATE PASSWORD
            if (request.Password != null && request.Password == request.ConfirmPassword)
            {
                var token = await _userManager.GeneratePasswordResetTokenAsync(user);
                var result = await _userManager.ResetPasswordAsync(user, token, request.Password);
                if (!result.Succeeded)
                {
                    throw new Exception(result.Errors.First().Description);
                }
            }
            if (request.Password != request.ConfirmPassword)
            {
                throw new Exception("Sifre se ne podudaraju!");
            }

            //UPDATE ROLE
            var userRole = await GetRolesByUserId(id);
            //Check role
            var roleExist = DoesRoleExist(request.RoleName);

            if (!roleExist)
            {
                throw new Exception("Rola koju ste unijeli ne postoji!");
            }
            if (userRole != request.RoleName)
            {
                await _userManager.RemoveFromRoleAsync(user, userRole);
                await _userManager.AddToRoleAsync(user, request.RoleName);
            }
            _applicationDbContext.SaveChanges();

            return user;
        }


        public async Task<UserEnity> DeleteUser(int id)
        {
            var user = GetUserById(id);

            if (user == null)
            {
                throw new Exception("Korisnik nije pronadjen");
            }

            repository.User.Delete(user);
            await _userManager.DeleteAsync(user);
            _applicationDbContext.SaveChanges();

            return user;
        }

        private async Task<string> GetRolesByUserId(int? id)
        {
            if(id != null)
            {
                var user = GetUserById(id.Value);
                if(user == null)
                {
                    throw new Exception("Korisnik nije pronadjen");
                }
                IList<string> roles = await _userManager.GetRolesAsync(user);

                return roles.First().ToString();
            }
            return null;
        }


        private bool DoesRoleExist(string roleName)
        {
            var roles = _applicationDbContext.Roles.ToList();
            foreach(var role in roles)
            {
                if(role.ToString() == roleName)
                {
                    return true;
                }
            }
            return false;
        }


    }
}
