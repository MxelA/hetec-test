﻿using WebApp.Modules.UserManagement.Http.Requests;
using WebApp.Repository;
using System.Threading.Tasks;
using UserEntity = WebApp.Modules.UserManagement.Entities.User;

namespace WebApp.Modules.UserManagement.Repository.User
{
    public interface IUserRepository: IRepositoryBase<UserEntity> 
    {
        public UserRepository CriteriaByName(string name);
        public UserEntity GetUserById(int id);

        public Task<UserEntity> CreateUser(UserCreateRequest request);

        public Task<UserEntity> UpdateUser(int id,UserUpdateRequest request);

        public Task<UserEntity> DeleteUser(int id);
    }
}