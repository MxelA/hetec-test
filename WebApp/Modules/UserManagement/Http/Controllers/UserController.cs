﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using WebApp.Request;
using WebApp.Response;
using WebApp.Modules.UserManagement.Entities;
using WebApp.Modules.UserManagement.Http.Resources;
using WebApp.Repository;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using WebApp.Policies;
using WebApp.Modules.UserManagement.Http.Requests;

namespace WebApp.Modules.UserManagement.Http.Controllers
{

    [ApiController]
    [Route("[controller]")]

    public class UserController : ControllerBase
    {
        private readonly IRepositoryWrapper _repository;
        private readonly IPolicyWrapper _policy;
        private readonly IMapper _mapper;

    
        public UserController(IRepositoryWrapper repository, IMapper mapper, IPolicyWrapper policyWrapper)
        {
            _repository = repository;
            _policy     = policyWrapper;
            _mapper = mapper;
        }

        /// <summary>
        /// Get Users.
        /// </summary>
        [HttpGet("GetUsers")]
        public IActionResult GetUsers([FromQuery] PaginationParameterRequest paginationParameter)
        {
            _policy.UserPolicy.CanUserManage();

            return Ok(
                new ServiceResponse<PaginateListResource<UserResource>>(){
                    Data = _mapper.Map<PaginateListResource<UserResource>>(_repository.User.Paginate(paginationParameter)) 
                }
            );
        }



        [HttpGet("GetUser")]
        public IActionResult GetUser([FromQuery] int id)
        {
            //_policy.UserPolicy.CanUserManage();
            var user = _repository.User.GetUserById(id);

            return Ok(
                new ServiceResponse<UserResource>()
                {
                    Data = _mapper.Map<UserResource>(user)
                }
            );
        }

        [HttpPost("CreateUser")]
        public async Task<IActionResult> CreateUser([FromBody] UserCreateRequest request)
        {
            var user = await _repository.User.CreateUser(request);

            return Ok(
                new ServiceResponse<UserResource>()
                {
                    Data = _mapper.Map<UserResource>(user)
                }
            );
        }


        [HttpPut("UpdateUser")]
        public async Task<IActionResult> UpdateUser([FromQuery]int id,[FromBody] UserUpdateRequest request)
        {
            //_policy.UserPolicy.CanUserManage();
            var user = await _repository.User.UpdateUser(id, request);

            return Ok(
                new ServiceResponse<UserResource>()
                {
                    Data = _mapper.Map<UserResource>(user)
                }
            );
        }


        [HttpDelete("DeleteUser")]
        public async Task<IActionResult> DeleteUser([FromQuery] int id)
        {
            //_policy.UserPolicy.CanUserManage();
            var user = await _repository.User.DeleteUser(id);

            return Ok(
                new ServiceResponse<string>()
                {
                    Data = _mapper.Map<string>("Korisnik je uspjesno obrisan")
                }
            );
        }

    }
}
