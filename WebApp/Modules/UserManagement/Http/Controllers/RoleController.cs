﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using AutoMapper;
using WebApp.Modules.UserManagement.CustomValidations;
using WebApp.Modules.UserManagement.Entities;
using WebApp.Modules.UserManagement.Http.Resources;
using WebApp.Policies;
using WebApp.Repository;
using WebApp.Request;
using WebApp.Response;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace WebApp.Modules.UserManagement.Http.Controllers
{
    [Route("/[controller]")]
    [ApiController]
    public class RoleController : ControllerBase
    {


        RoleManager<Role> _roleManager;
        private readonly IRepositoryWrapper _repository;
        private readonly IMapper _mapper;
        private readonly IPolicyWrapper _policyWrapper;

        public RoleController(IRepositoryWrapper repository, IMapper mapper, IPolicyWrapper policyWrapper, RoleManager<Role> roleManager)
        {
            _repository = repository;
            _mapper = mapper;
            _policyWrapper = policyWrapper;
            _roleManager = roleManager;
        }


        // GET: api/<PermissionController>
        [HttpPost("CreateRole")]
        [Authorize]
        public async Task<IActionResult> CreateRole([FromBody] CreateRoleRequest request)
        {

            Role identityRole = new Role
            {
                Name = request.RoleName,
            };

            var roleCreated = await _roleManager.CreateAsync(identityRole);

            if (roleCreated.Succeeded)
            {
                foreach (var permission in request.Permissions)
                {
                    var claim = new Claim("app", permission.ToString());
                    await _roleManager.AddClaimAsync(identityRole, claim);
                }
            }

            return Ok(
                new ServiceResponse<RoleResource>()
                {
                    Data = _mapper.Map<RoleResource>(identityRole)
                }
                );
        }


        [HttpDelete("DeleteRole")]
        [Authorize]
        public async Task<IActionResult> DeleteRole(int id)
        {
            var rola = await _roleManager.FindByIdAsync(id.ToString());

            if(rola != null)
            {
               await _roleManager.DeleteAsync(rola);

                return Ok(
                    new ServiceResponse<string>()
                    {
                        Data = "Rola uspjesno uklonjena!"
                    }
                    );
            }else
            {
                return NotFound(new ServiceResponse<string>()
                {
                    Data = "Rola s tim imenom nije pronadjena!"
                });
            }

 
        }

        [HttpPut("UpdateRole")]
        [Authorize]
        public async Task<IActionResult> UpdateRole(UpdateRoleRequest request)
        {
            var rola = await _roleManager.FindByIdAsync(request.Id);
            var claims = await _roleManager.GetClaimsAsync(rola);

            if(rola != null)
            {
                rola.Name = request.RoleName;
                foreach (var claim in claims)
                {
                    await _roleManager.RemoveClaimAsync(rola, claim);
                }
                foreach (var permission in request.Permissions)
                {
                    var claim = new Claim("app", permission.ToString());
                    await _roleManager.AddClaimAsync(rola, claim);
                }
            }

            return Ok(
            new ServiceResponse<RoleResource>()
            {
                Data = _mapper.Map<RoleResource>(rola)
            }
            );
        }
    }
}
