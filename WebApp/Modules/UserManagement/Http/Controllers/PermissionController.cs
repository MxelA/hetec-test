﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using AutoMapper;
using WebApp.Modules.UserManagement.Entities;
using WebApp.Modules.UserManagement.Http.Resources;
using WebApp.Modules.UserManagement.Policies.Permission;
using WebApp.Policies;
using WebApp.Repository;
using WebApp.Request;
using WebApp.Response;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace WebApp.Modules.UserManagement.Http.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class PermissionController : ControllerBase
    {
        RoleManager<Role> _roleManager;
        private readonly IRepositoryWrapper _repository;
        private readonly IMapper _mapper;
        private readonly IPolicyWrapper _policyWrapper;

        public PermissionController(IRepositoryWrapper repository, IMapper mapper, IPolicyWrapper policyWrapper, RoleManager<Role> roleManager)
        {
            _repository = repository;
            _mapper = mapper;
            _policyWrapper = policyWrapper;
            _roleManager = roleManager;
        }


        // GET: api/<PermissionController>
        [HttpGet("GetPermissions")]
        public IActionResult GetPermissions([FromQuery] PaginationParameterRequest paginationParameter)
        {
            _policyWrapper.PermissionPolicy.CanPermissionView();
            
            return Ok(
                new ServiceResponse<PaginateListResource<ClaimsResource>>()
                {
                    Data = _mapper.Map<PaginateListResource<ClaimsResource>>(_repository.Permission.Paginate(paginationParameter))
                }
            );
        }



       

    }
}
