using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using WebApp.Modules.UserManagement.Http.Requests;
using WebApp.Repository;
using Microsoft.AspNetCore.Authorization;

namespace WebApp.Modules.UserManagement.Http.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class AuthController : ControllerBase
    {

        protected readonly IRepositoryWrapper _repository;
        public AuthController(IRepositoryWrapper repository)
        {
            _repository = repository;
        }

///<remarks>
/// Sample request:
///
///     POST /Todo
///     {
///        "email": "milicalex@gmail.com",
///        "password": "SuperSecret!!2021"
///     }
///
/// </remarks>
        [HttpPost("login")]
        [AllowAnonymous]
        public async Task<IActionResult> Login([FromBody] LoginRequest loginRequest)
        {
            return Ok(await _repository.Auth.GetUserJwt(loginRequest));
        }
    }
}