namespace WebApp.Modules.UserManagement.Http.Resources
{
    public class LoginResource
    {
        public string FirstName {get; set; }
        public string LastName {get; set;}
        public string Token {get; set;}
    }
}