﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApp.Modules.UserManagement.Http.Resources
{
    public class ClaimsResource
    {
        public string Name { get; set; }
    }
}
