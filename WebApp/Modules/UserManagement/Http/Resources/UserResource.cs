
using System.Collections.Generic;
using WebApp.Modules.UserManagement.Entities;

namespace WebApp.Modules.UserManagement.Http.Resources
{
    public class UserResource
    {
        public int Id { get; set; }
        public string FirstName {get; set; }
        public string LastName {get; set;}
        public string Email { get; set; }
       
    }
}