﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApp.Modules.UserManagement.Http.Resources
{
    public class RoleResource
    {
        public string Name { get; set; }

        public string Id { get; set; }

        public List<string> Permissions { get; set; }
    }
}
