﻿using WebApp.Modules.UserManagement.Http.Resources;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace WebApp.Modules.UserManagement.Http.Requests
{
    public class UserUpdateRequest
    {

        public string FirstName { get; set; }

        public string LastName { get; set; }

        [Required]
        [EmailAddress]
        public string Email { get; set; }

        public string Password { get; set; }

        public string ConfirmPassword { get; set; }

        public string RoleName { get; set; }
    }
}
