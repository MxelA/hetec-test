﻿
using WebApp.Modules.UserManagement.Entities;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using System;
using WebApp.Entities.Context;

namespace WebApp.Modules.UserManagement.Seeders
{
    public static class RoleSeed
    {
        public static void Seed(ModelBuilder builder)
        {

            builder.Entity<Role>().HasData(new Role(){
                Id   = 1,
                Name = "Administrator",
                NormalizedName = "ADMINISTRATOR",
                ConcurrencyStamp = Guid.NewGuid().ToString()   
            });

            builder.Entity<RoleClaim>().HasData(new []{
                new RoleClaim(){
                    Id = 1,
                    RoleId     = 1,
                    ClaimType  = "app",
                    ClaimValue = Claims.RoleView
                },
                new RoleClaim(){
                    Id = 2,
                    RoleId     = 1,
                    ClaimType  = "app",
                    ClaimValue = Claims.RoleManage
                },
                new RoleClaim(){
                    Id = 3,
                    RoleId     = 1,
                    ClaimType  = "app",
                    ClaimValue = Claims.PermissionView
                },
                new RoleClaim(){
                    Id = 4,
                    RoleId     = 1,
                    ClaimType  = "app",
                    ClaimValue = Claims.UserView
                },
                new RoleClaim(){
                    Id = 5,
                    RoleId     = 1,
                    ClaimType  = "app",
                    ClaimValue = Claims.UserManage
                },
                new RoleClaim(){
                    Id = 6,
                    RoleId     = 1,
                    ClaimType  = "app",
                    ClaimValue = Claims.VehicleModelView
                },
                new RoleClaim(){
                    Id = 7,
                    RoleId     = 1,
                    ClaimType  = "app",
                    ClaimValue = Claims.VehicleView
                },
                new RoleClaim(){
                    Id = 8,
                    RoleId     = 1,
                    ClaimType  = "app",
                    ClaimValue = Claims.VehicleManage
                }
            }); 
        }

        public static void Seed(ApplicationDbContext context)
        {

            context.Role.Add(new Role()
            {
                Id = 1,
                Name = "Administrator",
                NormalizedName = "ADMINISTRATOR",
                ConcurrencyStamp = Guid.NewGuid().ToString()
            });

            context.RoleClaim.AddRange(new[]{
                new RoleClaim(){
                    Id = 1,
                    RoleId     = 1,
                    ClaimType  = "app",
                    ClaimValue = Claims.RoleView
                },
                new RoleClaim(){
                    Id = 2,
                    RoleId     = 1,
                    ClaimType  = "app",
                    ClaimValue = Claims.RoleManage
                },
                new RoleClaim(){
                    Id = 3,
                    RoleId     = 1,
                    ClaimType  = "app",
                    ClaimValue = Claims.PermissionView
                },
                new RoleClaim(){
                    Id = 4,
                    RoleId     = 1,
                    ClaimType  = "app",
                    ClaimValue = Claims.UserView
                },
                new RoleClaim(){
                    Id = 5,
                    RoleId     = 1,
                    ClaimType  = "app",
                    ClaimValue = Claims.UserManage
                },
                new RoleClaim(){
                    Id = 6,
                    RoleId     = 1,
                    ClaimType  = "app",
                    ClaimValue = Claims.VehicleModelView
                },
                new RoleClaim(){
                    Id = 7,
                    RoleId     = 1,
                    ClaimType  = "app",
                    ClaimValue = Claims.VehicleView
                },
                new RoleClaim(){
                    Id = 8,
                    RoleId     = 1,
                    ClaimType  = "app",
                    ClaimValue = Claims.VehicleManage
                },
                new RoleClaim(){
                    Id = 9,
                    RoleId     = 1,
                    ClaimType  = "app",
                    ClaimValue = Claims.RaceView
                },
                new RoleClaim(){
                    Id = 10,
                    RoleId     = 1,
                    ClaimType  = "app",
                    ClaimValue = Claims.RaceManage
                },
            });


            context.SaveChanges();
        }

    }
}
