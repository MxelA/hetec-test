﻿
using WebApp.Modules.UserManagement.Entities;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using System;
using System.Threading.Tasks;
using WebApp.Entities.Context;

namespace WebApp.Modules.UserManagement.Seeders
{
    public static class UserSeed
    {
        public static void Seed(ModelBuilder builder)
        {
            //seed user
            PasswordHasher<User> ph = new PasswordHasher<User>();
            builder.Entity<User>().HasData(new User {
                Id = 1,
                Email = "milicalex@gmail.com",
                EmailConfirmed = true,
                FirstName = "Aleksandar",
                LastName = "Milic",
                UserName = "milicalex@gmail.com",
                NormalizedUserName = "MILICALEX@GMAIL.COM",
                NormalizedEmail = "MILICALEX@GMAIL.COM",
                PasswordHash = ph.HashPassword(null, "SuperSecret!!2021"),
                SecurityStamp = "SDf23fasrsdfg"

            });

            builder.Entity<UserRole>().HasData(new []{
                new UserRole(){
                    UserId = 1,
                    RoleId = 1
                }
            });
        }

        public static void Seed(ApplicationDbContext context)
        {

            PasswordHasher<User> ph = new PasswordHasher<User>();
            context.User.Add(new User()
            {
                Id = 1,
                Email = "milicalex@gmail.com",
                EmailConfirmed = true,
                FirstName = "Aleksandar",
                LastName = "Milic",
                UserName = "milicalex@gmail.com",
                NormalizedUserName = "MILICALEX@GMAIL.COM",
                NormalizedEmail = "MILICALEX@GMAIL.COM",
                PasswordHash = ph.HashPassword(null, "SuperSecret!!2021"),
                SecurityStamp = "SDf23fasrsdfg"
            });

            context.UserRole.AddRange(new[]{
                new UserRole(){
                    UserId = 1,
                    RoleId = 1
                }
            });

            context.SaveChanges();
        }

    }
}
