﻿using WebApp.Entities;
using WebApp.Modules.UserManagement.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using WebApp.Entities.Context;

namespace WebApp.Modules.UserManagement.Seeders
{
    public static class ClaimSeed
    {
        public static void Seed(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Claims>().HasData(new Claims
            {
                Id = 1,
                Name = Claims.PermissionView ,          
            }); 

            modelBuilder.Entity<Claims>().HasData(new Claims
            {
                Id = 2,
                Name = Claims.RoleManage          
            });

            modelBuilder.Entity<Claims>().HasData(new Claims
            {
                Id = 3,
                Name = Claims.RoleView           
            }); 

            modelBuilder.Entity<Claims>().HasData(new Claims
            {
                Id = 4,
                Name = Claims.UserManage         
            }); 
            
            modelBuilder.Entity<Claims>().HasData(new Claims
            {
                Id = 5,
                Name = Claims.UserView          
            }); 
            
            modelBuilder.Entity<Claims>().HasData(new Claims
            {
                Id = 6,
                Name = Claims.VehicleModelView          
            }); 
            
            modelBuilder.Entity<Claims>().HasData(new Claims
            {
                Id = 7,
                Name = Claims.VehicleView          
            }); 

            modelBuilder.Entity<Claims>().HasData(new Claims
            {
                Id = 8,
                Name = Claims.VehicleManage          
            }); 

             modelBuilder.Entity<Claims>().HasData(new Claims
            {
                Id = 9,
                Name = Claims.RaceView          
            }); 
            
             modelBuilder.Entity<Claims>().HasData(new Claims
            {
                Id = 10,
                Name = Claims.RaceManage          
            }); 
            

        }

        public static void Seed(ApplicationDbContext context)
        {
            context.Claims.AddRange(new [] {
                new Claims
                {
                    Id = 1,
                    Name = Claims.PermissionView,
                },
                new Claims
                {
                    Id = 2,
                    Name = Claims.RoleManage
                },
                new Claims
                {
                    Id = 3,
                    Name = Claims.RoleView
                },
                new Claims
                {
                    Id = 4,
                    Name = Claims.UserManage
                },
                new Claims
                {
                    Id = 5,
                    Name = Claims.UserView
                },
                new Claims
                {
                    Id = 6,
                    Name = Claims.VehicleModelView
                },
                new Claims
                {
                    Id = 7,
                    Name = Claims.VehicleView
                },
                new Claims
                {
                    Id = 8,
                    Name = Claims.VehicleManage
                },
                new Claims
                {
                    Id = 9,
                    Name = Claims.RaceView
                },
                new Claims
                {
                    Id = 10,
                    Name = Claims.RaceManage
                }
            });
            
            context.SaveChanges();


        }
    }
}
