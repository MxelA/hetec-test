﻿using WebApp.Exceptions;
using WebApp.Modules.UserManagement.Entities;
using WebApp.Policies;
using WebApp.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApp.Modules.UserManagement.Policies.Permission
{
    public class PermissionPolicy : BasePolicy, IPermissionPolicy
    {
        private readonly IRepositoryWrapper _repository;
        public PermissionPolicy(IRepositoryWrapper repository) : base(repository)
        {
            _repository = repository;
        }


        public bool CanPermissionView()
        {
            if(AuthUserClaims == null)
            {
                throw new HttpException(403, "Forbidden");
            }

            var claim = AuthUserClaims.Where(p => p.ClaimValue.Equals(Claims.PermissionView)).FirstOrDefault();

            if (claim == null)
            {
                throw new HttpException(403, "Forbidden");
            }

            return true;
        }
    }
}
