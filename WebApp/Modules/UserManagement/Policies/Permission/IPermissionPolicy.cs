﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApp.Modules.UserManagement.Policies.Permission
{
    public interface IPermissionPolicy
    {
        bool CanPermissionView();
    }
}
