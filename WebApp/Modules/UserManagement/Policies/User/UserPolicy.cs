using System.Linq;
using WebApp.Exceptions;
using WebApp.Modules.UserManagement.Entities;
using WebApp.Policies;
using WebApp.Repository;
using UserEntity = WebApp.Modules.UserManagement.Entities.User;

namespace WebApp.Modules.UserManagement.Policies.User
{
    public class UserPolicy:BasePolicy, IUserPolicy
    {
        private readonly IRepositoryWrapper _repository;
        public UserPolicy(IRepositoryWrapper repository) :base(repository)
        {
            _repository = repository;
        }
        public bool CanUserManage()
        {
            var claim = AuthUserClaims.Where(p => p.ClaimValue.Equals(Claims.UserManage)).FirstOrDefault();
            
            if(claim == null) {
                throw new HttpException(401, "Unauthorized");
            }

            return true;
        }

        public bool CanUserView()
        {
            var claim = AuthUserClaims.Where(p => p.ClaimValue.Equals(Claims.UserView)).FirstOrDefault();
            
            if(claim == null) {
                throw new HttpException(401, "Unauthorized");
            }

            return true;
        }
    }
}