using UserEntity = WebApp.Modules.UserManagement.Entities.User;
namespace WebApp.Modules.UserManagement.Policies.User
{
    public interface IUserPolicy
    {
        bool  CanUserManage();
        bool CanUserView();
    }
}