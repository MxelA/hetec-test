using System;
using System.Linq;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using WebApp.Request;

namespace WebApp.Response
{
    public class ServicePaginateList<T>
    {
        public int CurrentPage { get; private set; }
        public int TotalPages { get; private set; }
        public int PageSize { get; private set; }
        public int TotalCount { get; private set; }
        public bool HasPrevious => CurrentPage > 1;
        public bool HasNext => CurrentPage < TotalPages;
 		public List<T> Items { get; set; }
        public ServicePaginateList(List<T> items, int count, int pageNumber, int pageSize)
        {
            TotalCount 	= count;
            PageSize 	= pageSize;
            CurrentPage = pageNumber;
            TotalPages 	= (int)Math.Ceiling(count / (double)pageSize);
            Items		= items;
        }

        public static ServicePaginateList<T> ToPaginateList(IQueryable<T> source, PaginationParameterRequest paginationQueryParameters)
        {
            var count = source.Count();
            var items = source.Skip((paginationQueryParameters.PageNumber - 1) * paginationQueryParameters.PageSize).Take(paginationQueryParameters.PageSize).ToList();

            return new ServicePaginateList<T>(items, count, paginationQueryParameters.PageNumber, paginationQueryParameters.PageSize);
        }

		public static async Task<ServicePaginateList<T>> ToPaginateListAsync(IQueryable<T> source, PaginationParameterRequest paginationQueryParameters)
        {
            var count = source.Count();
            var items = await source.Skip((paginationQueryParameters.PageNumber - 1) * paginationQueryParameters.PageSize).Take(paginationQueryParameters.PageSize).ToListAsync();

            return new ServicePaginateList<T>(items, count, paginationQueryParameters.PageNumber, paginationQueryParameters.PageSize);
        }
    }
}