using Newtonsoft.Json;
namespace WebApp.Response
{
    public class ServiceResponse<T>
    {
        public T Data { get; set; }

        public override string ToString()
        {
            return JsonConvert.SerializeObject(this);
        }
    }
}