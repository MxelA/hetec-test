using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;
using System.Reflection;
using WebApp.Entities.Context;
using WebApp.Exceptions;
using WebApp.Response;
using WebApp.Request;

namespace WebApp.Repository
{
    public abstract class RepositoryBase<T> : IRepositoryBase<T> where T : class
    {
        protected IQueryable<T> model;
        protected ApplicationDbContext AplictaionContext { get; set; }
 
        public RepositoryBase(ApplicationDbContext context)
        {
            this.AplictaionContext = context;
            this.model = context.Set<T>();
            this.WithNotTrashData();
        }

        public IQueryable<T> GetNewIQueryableModel()
        {
            return this.AplictaionContext.Set<T>();
        }

        public IQueryable<T> GetIQueryableModel()
        {
            return this.model;
        }
        public IList<T> Get()
        {
            return this.model.ToList();
        }
        public T GetFirst()
        {
            return this.model.FirstOrDefault();
        }
        public T GetFirstOrFail()
        {
            var Model = this.model.FirstOrDefault();
            
            if(Model == null) {
                throw new HttpException(402, "Not Found");
            }

            return Model;
        }
        public async Task<IList<T>> GetAsync()
        {
            return await this.model.ToListAsync();
        }
        public async Task<T> GetFirstAsync()
        {
            return await this.model.FirstOrDefaultAsync();
        }
        public async Task<T> GetFirstOrFailAsync()
        {
            var Model = await this.model.FirstOrDefaultAsync(); 

            if(Model == null) {
                throw new HttpException(402, "Not found");
            }

            return Model;
            
        }
 
        public IRepositoryBase<T> ByCondition(Expression<Func<T, bool>> expression)
        {
            this.model = this.model.Where(expression); 

            return this;
        }

        public IRepositoryBase<T> Include(string parametar)
        {
            this.model = this.model.Include(parametar);

            return this;
        }

        public IRepositoryBase<T> WithTrashData()
        {
            this.model = this.GetNewIQueryableModel();

            return this;
        }

        public ServicePaginateList<T> Paginate(PaginationParameterRequest paginationParameter)
        {
            return ServicePaginateList<T>.ToPaginateList(this.model, paginationParameter);
        }
 
        public void Create(T entity)
        {
            PropertyInfo CreatedAt = entity.GetType().GetProperty("CreatedAt", BindingFlags.Public | BindingFlags.Instance);
            PropertyInfo UpdatedAt = entity.GetType().GetProperty("UpdatedAt", BindingFlags.Public | BindingFlags.Instance);

            if(CreatedAt != null && CreatedAt.CanWrite)
            {
                CreatedAt.SetValue(entity, DateTime.UtcNow, null);
            }

            if(UpdatedAt != null && UpdatedAt.CanWrite)
            {
                UpdatedAt.SetValue(entity, DateTime.UtcNow, null);
            }

            this.AplictaionContext.Set<T>().Add(entity);
        }

        public async Task CreateAsync(T entity)
        {
            PropertyInfo CreatedAt = entity.GetType().GetProperty("CreatedAt", BindingFlags.Public | BindingFlags.Instance);
            PropertyInfo UpdatedAt = entity.GetType().GetProperty("UpdatedAt", BindingFlags.Public | BindingFlags.Instance);

            if(CreatedAt != null && CreatedAt.CanWrite)
            {
                CreatedAt.SetValue(entity, DateTime.UtcNow, null);
            }

            if(UpdatedAt != null && UpdatedAt.CanWrite)
            {
                UpdatedAt.SetValue(entity, DateTime.UtcNow, null);
            }

            await this.AplictaionContext.Set<T>().AddAsync(entity);
        }
 
        public void Update(T entity)
        {
            PropertyInfo UpdatedAt = entity.GetType().GetProperty("UpdatedAt", BindingFlags.Public | BindingFlags.Instance);

            if(UpdatedAt != null && UpdatedAt.CanWrite)
            {
                UpdatedAt.SetValue(entity, DateTime.UtcNow, null);
            }

            this.AplictaionContext.Set<T>().Update(entity);
        }
 
        public void Delete(T entity)
        {
            PropertyInfo SoftDelete = entity.GetType().GetProperty("SoftDelete", BindingFlags.Public | BindingFlags.Instance);
            PropertyInfo DeletedAt  = entity.GetType().GetProperty("DeletedAt", BindingFlags.Public | BindingFlags.Instance);

            if(SoftDelete != null && (bool) SoftDelete.GetValue(entity, null) == true ) 
            {
                if(DeletedAt != null && DeletedAt.CanWrite) 
                {
                    DeletedAt.SetValue(entity, DateTime.UtcNow, null);
                    Update(entity); 
                }

            } else {
                this.AplictaionContext.Set<T>().Remove(entity);
            }
            
        }

        private void WithNotTrashData() 
        {
            PropertyInfo DeletedAt = typeof(T).GetProperty("DeletedAt", BindingFlags.Public | BindingFlags.Instance);
            if(DeletedAt != null) {
                var pe = Expression.Parameter(typeof(T));
                var property = Expression.PropertyOrField(pe, "DeletedAt");
                var value = Expression.Constant(null);

                var predicateBody = Expression.Equal(
                    property,
                    value
                );

                var whereCallExpression = Expression.Call(
                    typeof(Queryable),
                    "Where",
                    new[] { typeof(T) },
                    this.model.Expression,
                    Expression.Lambda<Func<T, bool>>(predicateBody, new ParameterExpression[] { pe })
                );

                this.model = this.model.Provider.CreateQuery<T>(whereCallExpression);
            }
            
        }
        
    }
}