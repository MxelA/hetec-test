using Microsoft.Extensions.Configuration;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using System.Threading.Tasks;
using WebApp.Entities.Context;
using WebApp.Modules.UserManagement.Entities;
using WebApp.Modules.UserManagement.Repository.User;
using WebApp.Modules.UserManagement.Repository.Auth;
using WebApp.Modules.UserManagement.Repository.PermissionRepo;
using WebApp.Modules.DakarRally.Repository.VehicleModel;
using WebApp.Modules.DakarRally.Repository.Vehicle;
using WebApp.Modules.DakarRally.Repository.Race;
using WebApp.Modules.DakarRally.Repository.VehicleRace;

namespace WebApp.Repository
{
    public class RepositoryWrapper: IRepositoryWrapper
    {
        private ApplicationDbContext _applicationDbContext;
        
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly IConfiguration _configuration;
        private readonly SignInManager<User> _signInManager;
        private readonly UserManager<User> _userManager;
        // private IAuthRepository _auth;

        public RepositoryWrapper(
            ApplicationDbContext applicationContext, 
            IConfiguration configuration, 
            IHttpContextAccessor httpContextAccessor, 
            SignInManager<User> signInManager, 
            UserManager<User> userManager
        ){
            _applicationDbContext     = applicationContext;
            _httpContextAccessor    = httpContextAccessor;
            _configuration          = configuration;
            _signInManager          = signInManager;
            _userManager            = userManager;
        }       

        public void Save()
        {
            _applicationDbContext.SaveChanges();
        }

        public async Task SaveAsync()
        {
           await _applicationDbContext.SaveChangesAsync();
        }

        private IUserRepository _user;
        public IUserRepository User {
            get {
                if(_user == null)
                {
                    _user = new UserRepository(_applicationDbContext, this, _userManager);
                }
 
                return _user;
            }
        }

        private IAuthRepository _auth;
        public IAuthRepository Auth {
            get {
                if(_auth == null)
                {
                    _auth = new AuthRepository(_applicationDbContext, this, _configuration, _httpContextAccessor, _signInManager, _userManager);
                }
 
                return _auth;
            }
        }


        private IPermissionRepository _permission;
        public IPermissionRepository Permission
        {
            get
            {
                if (_permission == null)
                {
                    _permission = new PermissionRepository(_applicationDbContext, this, _configuration, _httpContextAccessor, _signInManager, _userManager);
                }
                return _permission;
            }

        }

        private IVehicleModelRepository _vehicleModel;
        public IVehicleModelRepository VehicleModel {
            get {
                
                if(_vehicleModel == null) {
                    _vehicleModel = new VehicleModelRepository(_applicationDbContext, this);
                }

                return _vehicleModel;
            }
        }

        private IVehicleRepository _vehicle;
        public IVehicleRepository Vehicle {
            get {
                if (_vehicle == null) {
                    _vehicle = new VehicleRepository(_applicationDbContext, this);
                }

                return _vehicle;
            }
        }

        private IRaceRepository _race;
        public IRaceRepository Race {
            get {
                if (_race == null) {
                    _race = new RaceRepository(_applicationDbContext, this);
                }

                return _race;
            }
        }

        private IVehicleRaceRepository _vehicleRace;
        public IVehicleRaceRepository VehicleRace {
            get {
                if(_vehicleRace == null) 
                {
                    _vehicleRace = new VehicleRaceRepository(_applicationDbContext, this);
                }

                return _vehicleRace;
            }
        }
    }
}