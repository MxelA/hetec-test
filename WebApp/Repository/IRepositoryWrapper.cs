using System.Threading.Tasks;
using WebApp.Modules.DakarRally.Repository.Race;
using WebApp.Modules.DakarRally.Repository.Vehicle;
using WebApp.Modules.DakarRally.Repository.VehicleModel;
using WebApp.Modules.DakarRally.Repository.VehicleRace;
using WebApp.Modules.UserManagement.Repository.Auth;
using WebApp.Modules.UserManagement.Repository.PermissionRepo;
using WebApp.Modules.UserManagement.Repository.User;

namespace WebApp.Repository
{
    public interface IRepositoryWrapper
    {  
        void Save();
        Task SaveAsync();

        IUserRepository User { get; }
        IAuthRepository Auth { get; }
        IPermissionRepository Permission { get; }
        IVehicleModelRepository VehicleModel { get; }
        IVehicleRepository Vehicle {get;}
        IRaceRepository Race {get;}
        IVehicleRaceRepository VehicleRace {get;}

    }
}