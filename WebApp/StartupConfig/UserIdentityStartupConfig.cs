using System.Text;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Configuration;
using WebApp.Modules.UserManagement.Entities;
using WebApp.Entities.Context;
using Microsoft.AspNetCore.Identity;

namespace WebApp.StartupConfig
{
    public static class UserIdentityStartupConfig
    {
        
        public static void UserIdentityConfigService(this IServiceCollection services)
        {
            services.AddIdentity<User, Role>()
                .AddEntityFrameworkStores<ApplicationDbContext>()
                .AddDefaultTokenProviders()
            ;
        }
    }
}