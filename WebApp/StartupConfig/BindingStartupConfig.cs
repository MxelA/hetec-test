using System.Text;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.AspNetCore.Authentication;
using WebApp.Repository;
using Microsoft.AspNetCore.Http;
using WebApp.Policies;
using WebApp.Modules.DakarRally.Services.RaceService;
using WebApp.Modules.DakarRally.Services.LeaderboardService;

namespace WebApp.StartupConfig
{
    public static class BindingStartupConfig
    {
        
        public static void BindingServices(this IServiceCollection services)
        {
            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>(); 
            services.AddScoped<IRepositoryWrapper, RepositoryWrapper>();
            services.AddScoped<IPolicyWrapper, PolicyWrapper>();
            services.AddScoped<IRaceService, RaceService>();
            services.AddScoped<ILeaderboardService, LeaderboardService>();
        }
    }
}