﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApp.Tests.Resources
{
    public class ExceptionResource
    {
        public string Message { get; set; }

        public string Data { get; set; }
    }
}
