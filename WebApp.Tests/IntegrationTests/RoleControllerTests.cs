﻿using FluentAssertions;
using WebApp.Modules.UserManagement.Entities;
using WebApp.Modules.UserManagement.Http.Resources;
using WebApp.Request;
using WebApp.Response;
using Microsoft.AspNetCore.Identity;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Xunit;
using NUnit.Framework;

namespace WebApp.Tests.IntegrationTests
{
   public class RoleControllerTests : IntegrationTest
    {
        #region CreateRole Tests
        [Fact]
        [TearDown]
        public async Task CreateRole_ShouldCreateRole()
        {
            // Arrange
            await AuthenticateAsync();
            List<string> Permissions = new List<string>();
            Permissions.Add("Role.View");
            Permissions.Add("Role.Manage");

            // Act
            var response = await TestClient.PostAsJsonAsync("Role/CreateRole", new CreateRoleRequest { 
                RoleName = "SomeRole",
                Permissions = Permissions
            });
            var content = response.Content.ReadAsStringAsync().Result;
            var readers = JsonConvert.DeserializeObject<ServiceResponse<RoleResource>>(content);
            var deleteUser = await TestClient.DeleteAsync($"Role/DeleteRole?id={readers.Data.Id}");
            // Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            await response.Content.ReadAsAsync<ServiceResponse<RoleResource>>();

        }


        [Fact]
        [TearDown]
        public async Task CreateRole_ShouldNotCreateRole_WhenPermissionDoesNotExistInDatabase()
        {
            // Arrange
            await AuthenticateAsync();
            List<string> Permissions = new List<string>();
            Permissions.Add("Role.View");
            Permissions.Add("WrongRole");

            // Act
            var response = await TestClient.PostAsJsonAsync("Role/CreateRole", new CreateRoleRequest
            {
                RoleName = "FakeRole",
                Permissions = Permissions
            });

            // Assert
            response.StatusCode.Should().Be(HttpStatusCode.BadRequest);

        }
        #endregion

        #region Delete Role Tests
        [Fact]
        public async Task DeleteRole_ShouldDeleteRole()
        {
            // Arrange

            await AuthenticateAsync();

            List<string> Permissions = new List<string>();

            var createRole = await TestClient.PostAsJsonAsync("Role/CreateRole", new CreateRoleRequest
            {
                RoleName = "DummyRole",
                Permissions = Permissions
            });

            var content = await createRole.Content.ReadAsStringAsync();
            var readers = JsonConvert.DeserializeObject<ServiceResponse<RoleResource>>(content);
            var url = $"https://localhost:44390/Role/DeleteRole?id={readers.Data.Id}";

            // Act 

            var response = await TestClient.DeleteAsync(url);

            // Assert

            response.StatusCode.Should().Be(HttpStatusCode.OK);
            await response.Content.ReadAsAsync<ServiceResponse<string>>();
        }


        [Fact]
        public async Task DeleteRole_ShouldNotDeleteRole_WhenItDoesNotExist()
        {
            // Arrange
            await AuthenticateAsync();
            var url = "https://localhost:44390/Role/DeleteRole?id=342545";

            // Act 

            var response = await TestClient.DeleteAsync(url);

            // Assert
            response.StatusCode.Should().Be(HttpStatusCode.NotFound);
        }
        #endregion

        //[Fact]
        //public async Task UpdateRole_ShouldUpdateRole()
        //{
        //    // Arrange
        //    await AuthenticateAsync();
        //    List<string> Permissions = new List<string>();
        //    Permissions.Add("Role.View");
        //    Permissions.Add("Role.Manage");

        //    var createRole = await TestClient.PostAsJsonAsync("Role/CreateRole", new CreateRoleRequest
        //    {
        //        RoleName = "ajbrale",
        //        Permissions = Permissions
        //    });

        //    var content = await createRole.Content.ReadAsStringAsync();
        //    var readers = JsonConvert.DeserializeObject<ServiceResponse<RoleResource>>(content);
        //    readers.Data.Name = "TestRole";
        //    readers.Data.Permissions.Add("User.Manage");
        //    // Act 

        //    var response = await TestClient.PutAsJsonAsync("Role/UpdateRole", readers.Data);

        //    // Assert
        //    response.StatusCode.Should().Be(HttpStatusCode.OK);
        //    await response.Content.ReadAsAsync<ServiceResponse<RoleResource>>();
        //}

        #region UpdateRole Tests
        [Fact]
        public async Task UpdateRole_ShouldNotUpdateRole_WhenThereIsWrongNameOfPermission()
        {
            // Arrange
            await AuthenticateAsync();
            List<string> Permissions = new List<string>();
            Permissions.Add("Fake.Permission");
            Permissions.Add("Non.Existent");
            // Act 

            var response = await TestClient.PutAsJsonAsync("Role/UpdateRole", new UpdateRoleRequest
            {
                Id = "3",
                RoleName = "FakeRole",
                Permissions = Permissions
            });

            // Assert
            response.StatusCode.Should().Be(HttpStatusCode.BadRequest);
            //await response.Content.ReadAsAsync<ServiceResponse<RoleResource>>();
        }
        #endregion




    }
}
