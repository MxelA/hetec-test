﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using FluentAssertions;
using WebApp.Modules.UserManagement.Entities;
using WebApp.Modules.UserManagement.Http.Resources;
using WebApp.Request;
using WebApp.Response;
using Newtonsoft.Json;
using Xunit;
using NUnit.Framework;

namespace WebApp.Tests
{
   public class PermissionControllerTests : IntegrationTest
    {
        
        #region GetPermissions_Tests

        [Fact]
        public async Task GetPermissions_ShouldReturnAllPermissions()
        {
            // Arrange
            await AuthenticateAsync();
            // Act
            var response = await TestClient.GetAsync("/api/Permission/GetPermissions");
            

            // Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            await response.Content.ReadAsAsync<ServiceResponse<PaginateListResource<ClaimsResource>>>();

           
        }

        [Fact]
        public async Task GetPermissions_ShouldReturnUnathorized_IfThereIsNoUser()
        {
            // Arrange

            // Act
            var response = await TestClient.GetAsync("/api/Permission/GetPermissions");


            // Assert
            response.StatusCode.Should().Be(HttpStatusCode.Unauthorized);
        }

        #endregion
    }
}
