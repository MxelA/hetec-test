﻿using FluentAssertions;
using WebApp.Middlewares;
using WebApp.Modules.UserManagement.Entities;
using WebApp.Modules.UserManagement.Http.Requests;
using WebApp.Modules.UserManagement.Http.Resources;
using WebApp.Response;
using WebApp.Tests.Resources;
using Microsoft.AspNetCore.Identity;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;
using Xunit;

namespace WebApp.Tests.IntegrationTests
{
   public class UserControllerTests : IntegrationTest
    {
        #region CreateUser_Tests
        [Fact]
        public async Task CreateUser_ShouldCreateUser()
        {
            // Arrange
            await AuthenticateAsync();
            // Act
            var response = await TestClient.PostAsJsonAsync("User/CreateUser", new UserCreateRequest
            {
               FirstName = "Aleksandar",
               LastName = "Josipovic",
               Email = "josa.sale@gmail.com",
               RoleName = "Administrator",
               Password = "SuperSecret!!2021",
               ConfirmPassword = "SuperSecret!!2021"
            });
            var content = response.Content.ReadAsStringAsync().Result;
            var readers = JsonConvert.DeserializeObject<ServiceResponse<UserResource>>(content);
            var deleteUser = await TestClient.DeleteAsync($"User/DeleteUser?id={readers.Data.Id}");
            //Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            await response.Content.ReadAsAsync<ServiceResponse<UserResource>>();
        }


        [Fact]
        public async Task CreateUser_ShouldNotCreateUser_WhenPasswordsMissmatch()
        {
            // Arrange
            await AuthenticateAsync();

            // Act
            var response = await TestClient.PostAsJsonAsync("User/CreateUser", new UserCreateRequest
            {
                FirstName = "Aleksandar",
                LastName = "Josipovic",
                Email = "josa.sale@gmail.com",
                RoleName = "Administrator",
                Password = "SuperSecret!!2021",
                ConfirmPassword = "SuperSecret!!2021D"
            });
            var content = response.Content.ReadAsStringAsync().Result;
            var exceptionMessage = JsonConvert.DeserializeObject<ExceptionResource>(content);

            //Assert
            response.StatusCode.Should().Be(HttpStatusCode.InternalServerError);
            Assert.Equal("Sifre se ne podudaraju!", exceptionMessage.Message);

        }

        [Fact]
        public async Task CreateUser_ShouldNotCreateUser_WhenEmailAlreadyExists()
        {
            // Arrange
            await AuthenticateAsync();

            // Act
            var response = await TestClient.PostAsJsonAsync("User/CreateUser", new UserCreateRequest
            {
                FirstName = "Aleksandar",
                LastName = "Milic",
                Email = "milicalex@gmail.com",
                RoleName = "Administrator",
                Password = "SuperSecret!!2021",
                ConfirmPassword = "SuperSecret!!2021"
            });
            var content = response.Content.ReadAsStringAsync().Result;
            var exceptionMessage = JsonConvert.DeserializeObject<ExceptionResource>(content);

            //Assert
            response.StatusCode.Should().Be(HttpStatusCode.InternalServerError);
            Assert.Equal("Korisnik vec postoji", exceptionMessage.Message);

        }

        [Fact]
        public async Task CreateUser_ShouldNotCreateUser_WhenRoleDoesNotExist()
        {
            // Arrange
            await AuthenticateAsync();

            // Act
            var response = await TestClient.PostAsJsonAsync("User/CreateUser", new UserCreateRequest
            {
                FirstName = "Milic",
                LastName = "Vukasinovic",
                Email = "glupiuser@novi.ba",
                RoleName = "RandomRola",
                Password = "SuperSecret!!2021",
                ConfirmPassword = "SuperSecret!!2021"
            });
            var content = response.Content.ReadAsStringAsync().Result;
            var exceptionMessage = JsonConvert.DeserializeObject<ExceptionResource>(content);

            //Assert
            response.StatusCode.Should().Be(HttpStatusCode.InternalServerError);
            Assert.Equal("Rola koju ste unijeli ne postoji!", exceptionMessage.Message);

        }
        #endregion


        #region UpdateUser_Tests
        


        [Fact]
        public async Task UpdateUser_ShouldNotUpdateUser_WhenRoleDoesNotExist()
        {
            // Arrange
            await AuthenticateAsync();
            var createUser = await TestClient.PostAsJsonAsync("User/CreateUser", new UserCreateRequest
            {
                FirstName = "Dummy",
                LastName = "User",
                Email = "should@update.com",
                RoleName = "Administrator",
                Password = "SuperSecret!!2021",
                ConfirmPassword = "SuperSecret!!2021"
            });

            var content = createUser.Content.ReadAsStringAsync().Result;
            var readers = JsonConvert.DeserializeObject<ServiceResponse<UserResource>>(content);
            readers.Data.FirstName = "Smarty";
            readers.Data.LastName = "User";
            readers.Data.Email = "updated@ba.com";
            // Act

            var response = await TestClient.PutAsJsonAsync($"User/UpdateUser?id={readers.Data.Id}", readers.Data);
            var deleteUser = await TestClient.DeleteAsync($"User/DeleteUser?id={readers.Data.Id}");
            var responseContent = response.Content.ReadAsStringAsync().Result;
            var exceptionMessage = JsonConvert.DeserializeObject<ExceptionResource>(responseContent);

            //Assert
            response.StatusCode.Should().Be(HttpStatusCode.InternalServerError);
            Assert.Equal("Rola koju ste unijeli ne postoji!", exceptionMessage.Message);

        }

        #endregion
    }
}
