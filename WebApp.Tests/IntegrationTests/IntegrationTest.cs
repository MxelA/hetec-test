﻿using WebApp.Entities.Context;
using WebApp.Modules.UserManagement.Http.Requests;
using WebApp.Modules.UserManagement.Http.Resources;
using Microsoft.AspNetCore.Mvc.Testing;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
using Newtonsoft.Json;
using System;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using Xunit;
using WebApp.Response;
using System.Linq;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Configuration;
using WebApp.Modules.UserManagement.Seeders;
using Microsoft.Extensions.Hosting;
using Microsoft.AspNetCore.Hosting;

namespace WebApp.Tests
{
    public class IntegrationTest
    {
        protected readonly HttpClient TestClient;
        protected bool seed = false;



        protected IntegrationTest()
        {
            var appFactory = new WebApplicationFactory<Startup>()
                .WithWebHostBuilder(builder =>
                {
                    //builder.UseEnvironment("Test");
                    builder.ConfigureServices(services =>
                    {
                        services.RemoveAll(typeof(ApplicationDbContext));

                        services.AddDbContext<ApplicationDbContext>(options =>
                        {
                            options.UseInMemoryDatabase(databaseName: Guid.NewGuid().ToString());
                            //options.UseSqlServer("Server=localhost;User Id=sa;Password=wjwotnbp1;Database=web_app_test;Trusted_Connection=False;");
                        });

                        var sp = services.BuildServiceProvider();
                        using (var scope = sp.CreateScope())
                        {
                            var scopedServices = scope.ServiceProvider;
                            var db = scopedServices.GetRequiredService<ApplicationDbContext>();
                            //db.Database.EnsureDeleted();
                            db.Database.EnsureCreated();
                            
                            try
                            {
                                ClaimSeed.Seed(db);
                                RoleSeed.Seed(db);
                                UserSeed.Seed(db);

                            } catch(Exception e) {

                            }



                            //VehicleModelSeeder.SeedOverApplicationDbContext(context);
                        }
                    });

                });
            TestClient = appFactory.CreateClient();
        }


        protected async Task AuthenticateAsync()
        {
            TestClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("bearer", await GetJwtAsync());
        }

        private async Task<string> GetJwtAsync()
        {
            var response = await TestClient.PostAsJsonAsync("/Auth/login", new LoginRequest
            {
                Email = "milicalex@gmail.com",
                Password = "SuperSecret!!2021"
            });

            var content = await response.Content.ReadAsStringAsync();
            var readers = JsonConvert.DeserializeObject<ServiceResponse<LoginResource>>(content);

            return readers.Data.Token;
        }
    }
}
